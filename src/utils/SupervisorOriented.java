package utils;

public class SupervisorOriented {

	private HelperMethods hm = new HelperMethods();
	
public String[] findSupervisorCode(String supervisor) {
		
		String[] supervisorCode = null;
		
		if ( supervisor.equalsIgnoreCase("Department of Defence") ) {
			supervisorCode =new String[]{ "000","Department of Defence","��������� ������"};
		} else if (supervisor.equalsIgnoreCase("Defence Materiel Organisation")) {
			supervisorCode =new String[]{"001","Defence Materiel Organisation","���������� ��������� ������"};
		} else if (supervisor.equalsIgnoreCase("Department of Foreign Affairs and Trade")) {
			supervisorCode =new String[]{"002","Department of Foreign Affairs and Trade","��������� ���������� ��� ��������"};
		} else if (supervisor.equalsIgnoreCase("Department of Human Services")) {
			supervisorCode =new String[]{"003","Department of Human Services","��������� ���������� ���������"};
		} else if ( supervisor.equalsIgnoreCase("Department of Health") ) {
			supervisorCode =new String[]{"004","Department of Health","��������� ������"};
		}  else if ( supervisor.equalsIgnoreCase("Australian Federal Police") ) {
			supervisorCode =new String[]{"005","Australian Federal Police","����������� ������������ ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of Education")) {
			supervisorCode =new String[]{"006","Department of Education","��������� ��������"};
		} else if (supervisor.equalsIgnoreCase(" Centrelink")) {
			supervisorCode =new String[]{"007"," Centrelink","���������"};
		} else if (supervisor.equalsIgnoreCase("Department of Immigration and Border Protection")) {
			supervisorCode =new String[]{"008","Department of Immigration and Border Protection","��������� ������������� ��� ���������� ��� �������"};
		} else if (supervisor.equalsIgnoreCase("Department of Foreign Affairs and Trade - Australian Aid Program")) {
			supervisorCode =new String[]{"009","Department of Foreign Affairs and Trade - Australian Aid Program","��������� ���������� ��� �������� - ����������� ��������� ��������"};
		} else if (supervisor.equalsIgnoreCase("Department of Education Employment and Workplace Relations")) {
			supervisorCode =new String[]{"010","Department of Education Employment and Workplace Relations","��������� �������� ����������� ��� ���������� �������"};
		} else if (supervisor.equalsIgnoreCase("Department of the Environment")) {
			supervisorCode =new String[]{"011","Department of the Environment","��������� �������������"};
		} else if (supervisor.equalsIgnoreCase("Australian Customs and Border Protection Service")) {
			supervisorCode =new String[]{"012","Australian Customs and Border Protection Service","����������� �������� ��������� ��� ���������� ��� �������"};
		} else if (supervisor.equalsIgnoreCase("Department of Industry")) {
			supervisorCode =new String[]{"013","Department of Industry","����� �����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Taxation Office")) {
			supervisorCode =new String[]{"014","Australian Taxation Office","����������� ������"};
		} else if (supervisor.equalsIgnoreCase("Attorney-General's Department")) {
			supervisorCode =new String[]{"015","Attorney-General's Department","����� ������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Department of Families Housing Community Services and Indigenous Affairs")) {
			supervisorCode =new String[]{"016","Department of Families Housing Community Services and Indigenous Affairs","��������� ������������� �������� ���������� ��������� ��� ��������� ��������"};
		} else if (supervisor.equalsIgnoreCase("Department of Finance")) {
			supervisorCode =new String[]{"017","Department of Finance","��������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Department of Agriculture")) {
			supervisorCode =new String[]{"018","Department of Agriculture","��������� ��������"};
		} else if (supervisor.equalsIgnoreCase("Australian Securities and Investments Commission")) {
			supervisorCode =new String[]{"019","Australian Securities and Investments Commission","����������� �������� ����� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Bureau of Meteorology")) {
			supervisorCode =new String[]{"020","Bureau of Meteorology","������� �������������"};
		} else if (supervisor.equalsIgnoreCase("Geoscience Australia")) {
			supervisorCode =new String[]{"021","Geoscience Australia","���-�������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Medicare Australia")) {
			supervisorCode =new String[]{"022","Medicare Australia",""};
		} else if (supervisor.equalsIgnoreCase("Department of Veterans' Affairs")) {
			supervisorCode =new String[]{"023","Department of Veterans' Affairs","����� ��������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of the Environment Water Heritage and the Arts")) {
			supervisorCode =new String[]{"024","Department of the Environment Water Heritage and the Arts","����� ��� ��������������� �������� ����������� ��� ��� ������"};
		} else if (supervisor.equalsIgnoreCase("Department of Parliamentary Services")) {
			supervisorCode =new String[]{"025","Department of Parliamentary Services","����� ���������������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Competition and Consumer Commission")) {
			supervisorCode =new String[]{"026","Australian Competition and Consumer Commission","����������� �������� ������������ ��� ���������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Austrade")) {
			supervisorCode =new String[]{"027","Austrade",""};
		} else if (supervisor.equalsIgnoreCase("Department of Communications")) {
			supervisorCode =new String[]{"028","Department of Communications","����� ������������"};
		} else if (supervisor.equalsIgnoreCase("Department of the Prime Minister and Cabinet")) {
			supervisorCode =new String[]{"029","Department of the Prime Minister and Cabinet","����� ��� ������������ ��� ��� ���������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Bureau of Statistics")) {
			supervisorCode =new String[]{"030","Australian Bureau of Statistics","����������� ���������� ��������"};
		} else if (supervisor.equalsIgnoreCase("Australian Antarctic Division")) {
			supervisorCode =new String[]{"031","Australian Antarctic Division","����������� ����� ��� �����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Electoral Commission")) {
			supervisorCode =new String[]{"032","Australian Electoral Commission","����������� �������� ��������"};
		} else if (supervisor.equalsIgnoreCase("Department of the Treasury")) {
			supervisorCode =new String[]{"033","Department of the Treasury","����� ���������������"};
		} else if (supervisor.equalsIgnoreCase("Department of Climate Change")) {
			supervisorCode =new String[]{"034","Department of Climate Change","����� ��� ���������� �������"};
		} else if (supervisor.equalsIgnoreCase("IP Australia")) {
			supervisorCode =new String[]{"035","IP Australia","IP ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Communications and Media Authority (ACMA)")) {
			supervisorCode =new String[]{"036","Australian Communications and Media Authority (ACMA)","����������� ���� ������������ ��� �����"};
		} else if (supervisor.equalsIgnoreCase("Department of Infrastructure Transport Regional Development and Local Government")) {
			supervisorCode =new String[]{"037","Department of Infrastructure Transport Regional Development and Local Government","��������� �������� ��������� ������������� ��������� ��� �������������"};
		} else if (supervisor.equalsIgnoreCase("Department of Resources Energy and Tourism")) {
			supervisorCode =new String[]{"038","Department of Resources Energy and Tourism","��������� ����������� ����� ��� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of Human Services Retired")) {
			supervisorCode =new String[]{"039","Department of Human Services Retired","����� ���������� ��������� ���� �������������"};
		} else if (supervisor.equalsIgnoreCase("Department of Employment")) {
			supervisorCode =new String[]{"040","Department of Employment","��������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Crime Commission")) {
			supervisorCode =new String[]{"041","Australian Crime Commission","����������� �������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Department of Climate Change and Energy Efficiency")) {
			supervisorCode =new String[]{"042","Department of Climate Change and Energy Efficiency","����� ��������� ������ ��� ��� ���������� �������������"};
		} else if (supervisor.equalsIgnoreCase("Department of Social Services")) {
			supervisorCode =new String[]{"043","Department of Social Services","����� ���������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of Infrastructure and Transport")) {
			supervisorCode =new String[]{"044","Department of Infrastructure and Transport","��������� �������� ��� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Public Service Commission")) {
			supervisorCode =new String[]{"045","Australian Public Service Commission","����������� �������� �������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of Regional Australia Local Government Arts and Sport")) {
			supervisorCode =new String[]{"046","Department of Regional Australia Local Government Arts and Sport","����� ������������� ��������� ������� ������������� ������ ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian National Audit Office (ANAO)")) {
			supervisorCode =new String[]{"047","Australian National Audit Office (ANAO)","����������� ������ ������� �������"};
		} else if (supervisor.equalsIgnoreCase("Australian Financial Security Authority")) {
			supervisorCode =new String[]{"048","Australian Financial Security Authority","����������� ���� ����������������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Prudential Regulation Authority (APRA)")) {
			supervisorCode =new String[]{"049","Australian Prudential Regulation Authority (APRA)","����������� ����������� ���� ����������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of Education Science and Training")) {
			supervisorCode =new String[]{"050","Department of Education Science and Training","��������� �������� ��������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("National Archives of Australia")) {
			supervisorCode =new String[]{"051","National Archives of Australia","������ ������ ��� ����������"};						
		} else if (supervisor.equalsIgnoreCase("Department of the Environment - Australian Antarctic Division")) {
			supervisorCode =new String[]{"052","Department of the Environment - Australian Antarctic Division","��������� ������������� - ������������ �������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Royal Australian Mint")) {
			supervisorCode =new String[]{"053","Royal Australian Mint","���������� �������� ���������������"};
		} else if (supervisor.equalsIgnoreCase("Department of Employment and Workplace Relations")) {
			supervisorCode =new String[]{"054","Department of Employment and Workplace Relations","����� ����������� ��� ���������� �������"};
		} else if (supervisor.equalsIgnoreCase("Therapeutic Goods Administration")) {
			supervisorCode =new String[]{"055","Therapeutic Goods Administration","�������� ������������ ������"};
		} else if (supervisor.equalsIgnoreCase("Civil Aviation Safety Authority")) {
			supervisorCode =new String[]{"056","Civil Aviation Safety Authority","���� ��������� ��������� ����������"};
		} else if (supervisor.equalsIgnoreCase("CrimTrac")) {
			supervisorCode =new String[]{"057","CrimTrac",""};
		} else if (supervisor.equalsIgnoreCase("Comsuper")) {
			supervisorCode =new String[]{"058","Comsuper",""};
		} else if (supervisor.equalsIgnoreCase("Office of the Fair Work Ombudsman")) {
			supervisorCode =new String[]{"059","Office of the Fair Work Ombudsman","������� ��� ��������� ��� ������"};
		} else if (supervisor.equalsIgnoreCase("Murray-Darling Basin Authority")) {
			supervisorCode =new String[]{"060","Murray-Darling Basin Authority",""};
		} else if (supervisor.equalsIgnoreCase("Family Court and Federal Circuit Court")) {
			supervisorCode =new String[]{"061","Family Court and Federal Circuit Court","������������ ���������� ��� ������������ ������������ ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Centre for International Agricultural Research")) {
			supervisorCode =new String[]{"062","Australian Centre for International Agricultural Research","����������� ������ ��� �� ������ �������� ������"};
		} else if (supervisor.equalsIgnoreCase("Department of Infrastructure and Regional Development")) {
			supervisorCode =new String[]{"063","Department of Infrastructure and Regional Development","����� �������� ��� ������������� ���������"};
		} else if (supervisor.equalsIgnoreCase("National Health and Medical Research Council")) {
			supervisorCode =new String[]{"064","National Health and Medical Research Council","������ ��������� �������� ������� ��� ������"};
		} else if (supervisor.equalsIgnoreCase("Australian Research Council")) {
			supervisorCode =new String[]{"065","Australian Research Council","����������� ��������� �������"};
		} else if (supervisor.equalsIgnoreCase("Great Barrier Reef Marine Park Authority")) {
			supervisorCode =new String[]{"066","Great Barrier Reef Marine Park Authority","���� ��������� ������ Great Barrier Reef"};
		} else if (supervisor.equalsIgnoreCase("Department of Transport and Regional Services")) {
			supervisorCode =new String[]{"067","Department of Transport and Regional Services","��������� ��������� ��� ������������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Fisheries Management Authority")) {
			supervisorCode =new String[]{"068","Australian Fisheries Management Authority","����������� ���� ����������� ��� �������"};
		} else if (supervisor.equalsIgnoreCase("Department of Finance and Administration")) {
			supervisorCode =new String[]{"069","Department of Finance and Administration","��������� ����������� ��� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Transaction Reports and Analysis Centre (AUSTRAC)")) {
			supervisorCode =new String[]{"070","Australian Transaction Reports and Analysis Centre (AUSTRAC)","����������� ������ �������� ��� �������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Clean Energy Regulator")) {
			supervisorCode =new String[]{"071","Clean Energy Regulator","���������� ���� ������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Radiation Protection and Nuclear Safety Agency (ARPANSA)")) {
			supervisorCode =new String[]{"072","Australian Radiation Protection and Nuclear Safety Agency (ARPANSA)","������������ ���������� ���������� ��� ��� ����������� ��� ��������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of the Environment and Water Resources")) {
			supervisorCode =new String[]{"073","Department of the Environment and Water Resources","��������� ������������� ��� �������� �����"};
		} else if (supervisor.equalsIgnoreCase("CRS Australia")) {
			supervisorCode =new String[]{"074","CRS Australia",""};
		} else if (supervisor.equalsIgnoreCase("Child Support Agency")) {
			supervisorCode =new String[]{"075","Child Support Agency","���������� �������� �����������"};
		} else if (supervisor.equalsIgnoreCase("National Capital Authority")) {
			supervisorCode =new String[]{"076","National Capital Authority","������ ���� �������������"};
		} else if (supervisor.equalsIgnoreCase("Department of Health - Therapeutic Goods Administration")) {
			supervisorCode =new String[]{"077","Department of Health - Therapeutic Goods Administration","��������� ������ - ��������� ������������ ������"};
		} else if (supervisor.equalsIgnoreCase("Department of Industry Tourism and Resources")) {
			supervisorCode =new String[]{"078","Department of Industry Tourism and Resources","��������� ����������� ��������� ��� �����"};
		} else if (supervisor.equalsIgnoreCase("Fair Work Commission")) {
			supervisorCode =new String[]{"079","Fair Work Commission","�������� ���������� �������"};
		} else if (supervisor.equalsIgnoreCase("Migration Review Tribunal and Refugee Review Tribunal (MRT-RRT)")) {
			supervisorCode =new String[]{"080","Migration Review Tribunal and Refugee Review Tribunal (MRT-RRT)","���������� ��������������� ������������ ��� ���������� ����������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Administrative Appeals Tribunal")) {
			supervisorCode =new String[]{"081","Administrative Appeals Tribunal","���������� �������"};
		} else if (supervisor.equalsIgnoreCase("Federal Circuit Court of Australia")) {
			supervisorCode =new String[]{"082","Federal Circuit Court of Australia","������������ ������������ ���������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("National Water Commission")) {
			supervisorCode =new String[]{"083","National Water Commission","������ �������� ������"};
		} else if (supervisor.equalsIgnoreCase("Old Parliament House")) {
			supervisorCode =new String[]{"084","Old Parliament House","����� �����"};
		} else if (supervisor.equalsIgnoreCase("Department of the House of Representatives")) {
			supervisorCode =new String[]{"085","Department of the House of Representatives","����� ��� ������ ��� ������������"};
		} else if (supervisor.equalsIgnoreCase("Department of Families Community Services & Indigenous Affairs")) {
			supervisorCode =new String[]{"086","Department of Families Community Services & Indigenous Affairs","����� ���������� ������������� ��������� & ��������� ��������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Fair Work Building Industry Inspectorate")) {
			supervisorCode =new String[]{"087","Office of the Fair Work Building Industry Inspectorate","������� ���������� ��������� ������� ����������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Industrial Registry")) {
			supervisorCode =new String[]{"088","Australian Industrial Registry","����������� ����������� ������"};
		} else if (supervisor.equalsIgnoreCase("Future Fund Management Agency")) {
			supervisorCode =new String[]{"089","Future Fund Management Agency","���������� ����������� ����������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Department of Communications Information Technology and the Arts")) {
			supervisorCode =new String[]{"090","Department of Communications Information Technology and the Arts","����� ������������ ������������ ��� ������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Official Secretary to the Governor-General")) {
			supervisorCode =new String[]{"091","Office of the Official Secretary to the Governor-General","������� ��� �������� ����������� ��� ������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Human Rights Commission")) {
			supervisorCode =new String[]{"092","Australian Human Rights Commission","����������� �������� ���������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Workplace Authority")) {
			supervisorCode =new String[]{"093","Workplace Authority","���� ���������� �����"};
		} else if (supervisor.equalsIgnoreCase("Australian Transport Safety Bureau")) {
			supervisorCode =new String[]{"094","Australian Transport Safety Bureau","����������� ������� ��������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of the Senate")) {
			supervisorCode =new String[]{"095","Department of the Senate","����� ��� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian Pesticides and Veterinary Medicines Authority")) {
			supervisorCode =new String[]{"096","Australian Pesticides and Veterinary Medicines Authority","����������� ���� ����������� ��� ������������ ��������"};
		} else if (supervisor.equalsIgnoreCase("Cancer Australia")) {
			supervisorCode =new String[]{"097","Cancer Australia",""};
		} else if (supervisor.equalsIgnoreCase("National Library of Australia")) {
			supervisorCode =new String[]{"098","National Library of Australia","������ ���������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Organ and Tissue Donation and Transplantation Authority")) {
			supervisorCode =new String[]{"099","Australian Organ and Tissue Donation and Transplantation Authority","����������� ���� ������ ������� ��� ������������� �����"};
		} else if (supervisor.equalsIgnoreCase("Office of National Assessments")) {
			supervisorCode =new String[]{"100","Office of National Assessments","������� ������� ������������"};
		} else if (supervisor.equalsIgnoreCase("Australian Institute of Family Studies")) {
			supervisorCode =new String[]{"101","Australian Institute of Family Studies","����������� ���������� ������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Office of Financial Management")) {
			supervisorCode =new String[]{"102","Australian Office of Financial Management","����������� ������� ����������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Tertiary Education Quality and Standards Agency")) {
			supervisorCode =new String[]{"103","Tertiary Education Quality and Standards Agency","���������� �������� ��� ��������;� ������������ �����������"};
		} else if (supervisor.equalsIgnoreCase("National Native Title Tribunal")) {
			supervisorCode =new String[]{"104","National Native Title Tribunal","������ ���������� ������ ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Sports Anti-Doping Authority (ASADA)")) {
			supervisorCode =new String[]{"105","Australian Sports Anti-Doping Authority (ASADA)","����������� ���� ��������� ����-��������"};
        } else if (supervisor.equalsIgnoreCase("CSIRO")) {
			supervisorCode =new String[]{"106","CSIRO",""};
		} else if (supervisor.equalsIgnoreCase("Questacon")) {
			supervisorCode =new String[]{"107","Questacon",""};
		} else if (supervisor.equalsIgnoreCase("Federal Court of Australia")) {
			supervisorCode =new String[]{"108","Federal Court of Australia","������������ ���������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Commonwealth Ombudsman")) {
			supervisorCode =new String[]{"109","Office of the Commonwealth Ombudsman","������� ��� ��������� ��� ������ ��� ��������������"};
		} else if (supervisor.equalsIgnoreCase("Department of Family and Community Services (FaCS)")) {
			supervisorCode =new String[]{"110","Department of Family and Community Services (FaCS)","����� ������������� ��� ���������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of Immigration and Multicultural and Indigenous Affairs (DIMIA)")) {
			supervisorCode =new String[]{"111","Department of Immigration and Multicultural and Indigenous Affairs (DIMIA)","��������� ������������� ��� ���������������� ��� �������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Safe Work Australia")) {
			supervisorCode =new String[]{"112","Safe Work Australia","������� ������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Department of Immigration and Multicultural Affairs")) {
			supervisorCode =new String[]{"113","Department of Immigration and Multicultural Affairs","��������� ������������� ��� ���������������� ���������"};						
		} else if (supervisor.equalsIgnoreCase("National Blood Authority")) {
			supervisorCode =new String[]{"114","National Blood Authority","������ ���� �������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Australian Information Commissioner")) {
			supervisorCode =new String[]{"115","Office of the Australian Information Commissioner","������� ��� ��������� ����������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("National Mental Health Commission")) {
			supervisorCode =new String[]{"116","National Mental Health Commission","������ �������� ������� ������"};
		} else if (supervisor.equalsIgnoreCase("Australian Commission for Law Enforcement Integrity")) {
			supervisorCode =new String[]{"117","Australian Commission for Law Enforcement Integrity","����������� �������� ��� ��� ������� ��� ����� ������������"};
		} else if (supervisor.equalsIgnoreCase("Department of the Prime Minister and Cabinet - APEC")) {
			supervisorCode =new String[]{"118","Department of the Prime Minister and Cabinet - APEC","����� ��� ������������ ��� ��� ���������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Professional Services Review")) {
			supervisorCode =new String[]{"119","Professional Services Review","��������� �������������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Department of the Environment and Heritage")) {
			supervisorCode =new String[]{"120","Department of the Environment and Heritage","��������� ������������� ��� ������������ �����������"};
		} else if (supervisor.equalsIgnoreCase("National Health Performance Authority")) {
			supervisorCode =new String[]{"121","National Health Performance Authority","������ ���� �������� ������"};
		} else if (supervisor.equalsIgnoreCase("Productivity Commission")) {
			supervisorCode =new String[]{"122","Productivity Commission","�������� ���������������"};
		} else if (supervisor.equalsIgnoreCase("Australian Fair Pay Commission")) {
			supervisorCode =new String[]{"123","Australian Fair Pay Commission","����������� �������� Fair Pay"};
		} else if (supervisor.equalsIgnoreCase("Office of the Director of Public Prosecutions")) {
			supervisorCode =new String[]{"124","Office of the Director of Public Prosecutions","������� ��� ��������� ��� �����������"};
		} else if (supervisor.equalsIgnoreCase("Australian National Preventive Health Agency")) {
			supervisorCode =new String[]{"125","Australian National Preventive Health Agency","������������ ������� ���������� ����������� ������"};
		} else if (supervisor.equalsIgnoreCase("Australian Aged Care Quality Agency")) {
			supervisorCode =new String[]{"126","Australian Aged Care Quality Agency","������������ ���������� ��������� �����������"};
		} else if (supervisor.equalsIgnoreCase("National Offshore Petroleum Safety and Environmental Management Authority")) {
			supervisorCode =new String[]{"127","National Offshore Petroleum Safety and Environmental Management Authority","������ ���� ��������� ��������� ���������� ��� ��������������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Office of Parliamentary Counsel")) {
			supervisorCode =new String[]{"128","Office of Parliamentary Counsel","������� ���������������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian War Memorial")) {
			supervisorCode =new String[]{"129","Australian War Memorial","����������� ������� �������"};
		} else if (supervisor.equalsIgnoreCase("Wheat Exports Australia")) {
			supervisorCode =new String[]{"130","Wheat Exports Australia","�������� �������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Commonwealth Grants Commission")) {
			supervisorCode =new String[]{"131","Commonwealth Grants Commission","��������������� �������� ��������"};
		} else if (supervisor.equalsIgnoreCase("Independent Hospital Pricing Authority")) {
			supervisorCode =new String[]{"132","Independent Hospital Pricing Authority","���������� ���� ������������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Telecommunications Universal Service Management Agency")) {
			supervisorCode =new String[]{"133","Telecommunications Universal Service Management Agency","���������� ����������� ��������������� ��������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Director of National Parks")) {
			supervisorCode =new String[]{"134","Director of National Parks","���������� ��� ������� ������"};
		} else if (supervisor.equalsIgnoreCase("National Competition Council")) {
			supervisorCode =new String[]{"135","National Competition Council","������ ��������� ������������"};
		} else if (supervisor.equalsIgnoreCase("Australian Maritime Safety Authority")) {
			supervisorCode =new String[]{"136","Australian Maritime Safety Authority","����������� ���� ��������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Grains Research and Development Corporation")) {
			supervisorCode =new String[]{"137","Grains Research and Development Corporation","�������� ������� ��� ��������� �������"};
		} else if (supervisor.equalsIgnoreCase("National Museum of Australia")) {
			supervisorCode =new String[]{"138","National Museum of Australia","������ ������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Parliamentary Budget Office")) {
			supervisorCode =new String[]{"139","Parliamentary Budget Officea","������� ���������������� ��������������"};
		} else if (supervisor.equalsIgnoreCase("Australian Skills Quality Authority")) {
			supervisorCode =new String[]{"140","Australian Skills Quality Authority","����������� ���� ��������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Inspector-General of Intelligence and Security")) {
			supervisorCode =new String[]{"141","Office of the Inspector-General of Intelligence and Security","������� ��� ������� ���������� ��� ��������� ����������� ��� ���������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Australian Accounting Standards Board")) {
			supervisorCode =new String[]{"142","Office of the Australian Accounting Standards Board","������� ��� ����������� ���������� ���������� �������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Broadcasting Corporation")) {
			supervisorCode =new String[]{"143","Australian Broadcasting Corporation","����������� ������� ������������ ������������"};						
		} else if (supervisor.equalsIgnoreCase("Australian Nuclear Science and Technology Organisation (ANSTO)")) {
			supervisorCode =new String[]{"144","Australian Nuclear Science and Technology Organisation (ANSTO)","������������ ���������� ��������� ��������� ��� �����������"};						
		} else if (supervisor.equalsIgnoreCase("Comcare")) {
			supervisorCode =new String[]{"145","Comcare",""};						
		} else if (supervisor.equalsIgnoreCase("Social Security Appeals Tribunal")) {
			supervisorCode =new String[]{"146","Social Security Appeals Tribunal","������� ���������� ���������"};						
		} else if (supervisor.equalsIgnoreCase("Reserve Bank of Australia")) {
			supervisorCode =new String[]{"147","Reserve Bank of Australia","����������� ������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Climate Change Authority")) {
			supervisorCode =new String[]{"148","Climate Change Authority","���� ���������� �������"};
		} else if (supervisor.equalsIgnoreCase("National Health Funding Body")) {
			supervisorCode =new String[]{"149","National Health Funding Body","������� ������ �������������� ������"};
		} else if (supervisor.equalsIgnoreCase("Australian Institute of Health and Welfare")) {
			supervisorCode =new String[]{"150","Australian Institute of Health and Welfare","����������� ���������� ������ ��� ��������"};						
		} else if (supervisor.equalsIgnoreCase("Workplace Gender Equality Agency")) {
			supervisorCode =new String[]{"151","Workplace Gender Equality Agency","�������� ��� ��� ������� ��� ����� ��� ���� ��������"};						
		} else if (supervisor.equalsIgnoreCase("Workplace Ombudsman")) {
			supervisorCode =new String[]{"152","Workplace Ombudsman","�������������� ���������� �����"};						
		} else if (supervisor.equalsIgnoreCase("Asbestos Safety and Eradication Agency")) {
			supervisorCode =new String[]{"153","Asbestos Safety and Eradication Agency","�������� ��������� ��� ��������� ��������"};						
		} else if (supervisor.equalsIgnoreCase("Tourism Australia")) {
			supervisorCode =new String[]{"154","Tourism Australia","��������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Auditing and Assurance Standards Board")) {
			supervisorCode =new String[]{"155","Office of the Auditing and Assurance Standards Board","������� ����������� ���������� ����������� �������� ��� ������������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Privacy Commissioner")) {
			supervisorCode =new String[]{"156","Office of the Privacy Commissioner","������� ��� ��������� ���������� ���������� ���������"};
		} else if (supervisor.equalsIgnoreCase("National Offshore Petroleum Safety Authority")) {
			supervisorCode =new String[]{"157","National Offshore Petroleum Safety Authority","������ ���� ��������� ��������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Law Reform Commission")) {
			supervisorCode =new String[]{"158","Australian Law Reform Commission","����������� �������� ������������� �������"};
		} else if (supervisor.equalsIgnoreCase("Australian Film Television and Radio School")) {
			supervisorCode =new String[]{"159","Australian Film Television and Radio School","����������� ����� ���������� ��� �����������"};
		} else if (supervisor.equalsIgnoreCase("Commonwealth of Australia")) {
			supervisorCode =new String[]{"160","Commonwealth of Australia","������������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Government Information Management Office")) {
			supervisorCode =new String[]{"161","Australian Government Information Management Office","����������� ������� ����������� ������������ �����������"};
		} else if (supervisor.equalsIgnoreCase("Equal Opportunity for Women in the Workplace Agency")) {
			supervisorCode =new String[]{"162","Equal Opportunity for Women in the Workplace Agency","���������� ����������� ���� ��������� ��� ��� �������� ���� ���� ��������"};
		} else if (supervisor.equalsIgnoreCase("Private Health Insurance Ombudsman")) {
			supervisorCode =new String[]{"163","Private Health Insurance Ombudsman","�������������� ��������� ��������� ������"};
		} else if (supervisor.equalsIgnoreCase("Inspector-General of Taxation")) {
			supervisorCode =new String[]{"164","Inspector-General of Taxation","������� ����������� ����������"};
		} else if (supervisor.equalsIgnoreCase("Australian Institute of Criminology")) {
			supervisorCode =new String[]{"165","Australian Institute of Criminology","����������� ���������� ���������������"};
		} else if (supervisor.equalsIgnoreCase("Office of the Renewable Energy Regulator")) {
			supervisorCode =new String[]{"166","Office of the Renewable Energy Regulator","������� ����������� ����� ����������� ���������"};
		} else if (supervisor.equalsIgnoreCase("Australian National Maritime Museum")) {
			supervisorCode =new String[]{"167","Australian National Maritime Museum","����������� ������ ������� �������"};
		} else if (supervisor.equalsIgnoreCase("Australia-Japan Foundation")) {
			supervisorCode =new String[]{"168","Australia-Japan Foundation","�����������-�������� ������"};
		} else if (supervisor.equalsIgnoreCase("Office of Workplace Services")) {
			supervisorCode =new String[]{"169","Office of Workplace Services","������� ��������� ���������� �����"};
		} else if (supervisor.equalsIgnoreCase("Office of the Australian Building and Construction Commissioner (ABCC)")) {
			supervisorCode =new String[]{"170","Office of the Australian Building and Construction Commissioner (ABCC)","����������� ������� ��������� ��������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Export Wheat Commission")) {
			supervisorCode =new String[]{"171","Export Wheat Commission","�������� ��� �������� ��������"};
		} else if (supervisor.equalsIgnoreCase("Dairy Adjustment Authority")) {
			supervisorCode =new String[]{"172","Dairy Adjustment Authority","���� �������������� �����������"};
		} else if (supervisor.equalsIgnoreCase("Export Finance and Insurance Corporation (EFIC)")) {
			supervisorCode =new String[]{"173","Export Finance and Insurance Corporation (EFIC)","�������� �������� ����������� ��� ���������"};
		} else if (supervisor.equalsIgnoreCase("National Gallery of Australia")) {
			supervisorCode =new String[]{"174","National Gallery of Australia","������ ���������� ��� ����������"};
		} else if (supervisor.equalsIgnoreCase("Sydney Harbour Federation Trust")) {
			supervisorCode =new String[]{"175","Sydney Harbour Federation Trust","���������� ��������� ���������� ��� �������� ��� ������"};
		} else if (supervisor.equalsIgnoreCase("Department of Agriculture and Water Resources")) {
			supervisorCode =new String[]{"176","Department of Agriculture and Water Resources",""};
		} else if (supervisor.equalsIgnoreCase("Department of Education and Training")) {
			supervisorCode =new String[]{"177","Department of Education and Training",""};
		} else if (supervisor.equalsIgnoreCase("Department of Industry, Innovation and Science")) {
			supervisorCode =new String[]{"178","Department of Industry, Innovation and Science",""};
		} else if (supervisor.equalsIgnoreCase("Department of Communications and the Arts")) {
			supervisorCode =new String[]{"179","Department of Communications and the Arts",""};
		} else if (supervisor.equalsIgnoreCase("Digital Transformation Office")) {
			supervisorCode =new String[]{"180","Digital Transformation Office",""};
		} else if (supervisor.equalsIgnoreCase("Department of Industry and Science")) {
			supervisorCode =new String[]{"181","Department of Industry and Science",""};
		} else if (supervisor.equalsIgnoreCase("Seacare")) {
			supervisorCode =new String[]{"182","Seacare",""};
		} else if (supervisor.equalsIgnoreCase("Department of Regional Australia, Local Government, Arts and Sport")) {
			supervisorCode =new String[]{"183","Department of Regional Australia, Local Government, Arts and Sport",""};
		} else if (supervisor.equalsIgnoreCase("Department of Families, Housing, Community Services and Indigenous Affairs")) {
			supervisorCode =new String[]{"184","Department of Families, Housing, Community Services and Indigenous Affairs",""};
		} else if (supervisor.equalsIgnoreCase("Department of Health and Ageing")) {
			supervisorCode =new String[]{"185","Department of Health and Ageing",""};
		} else if (supervisor.equalsIgnoreCase("Department of Industry, Innovation, Climate Change, Science, Research and Tertiary Education")) {
			supervisorCode =new String[]{"186","Department of Industry, Innovation, Climate Change, Science, Research and Tertiary Education",""};
		} else if (supervisor.equalsIgnoreCase("Department of Health and Ageing - Therapeutic Goods Administration")) {
			supervisorCode =new String[]{"187","Department of Health and Ageing - Therapeutic Goods Administration",""};
		} else if (supervisor.equalsIgnoreCase("Department of Sustainability, Environment, Water, Population and Communities")) {
			supervisorCode =new String[]{"188","Department of Sustainability, Environment, Water, Population and Communities",""};
		} else if (supervisor.equalsIgnoreCase("Department of Sustainability, Environment, Water, Population and Communities - Australian Antarctic Division")) {
			supervisorCode =new String[]{"189","Department of Sustainability, Environment, Water, Population and Communities - Australian Antarctic Division",""};
		} else if (supervisor.equalsIgnoreCase("Centrelink")) {
			supervisorCode =new String[]{"190","Centrelink",""};
		} else {
			//String row = Integer.toString(line);
			//supervisor = supervisor + "\t" + row;
			hm.writeUnknownMetadata("Supervisor",supervisor);
		}
		
		return supervisorCode;
}
	
}
