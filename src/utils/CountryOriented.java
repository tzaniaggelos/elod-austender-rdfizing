package utils;


/**
 * 
 * @author A. Tzanis
 *
 */
public class CountryOriented {

	
	private HelperMethods hm = new HelperMethods();
	
	
	/**
     * Find the the official abbreviation of the country.
     * 
     * @param String the country from the Excel file
     * @return String the official abbreviation of the country
     */
	public String[] findCountryAbbreviation(String country) {
		
		String[] abbreviation = null;
		
		if ( country.equalsIgnoreCase("Netherlands") ) {
			abbreviation =new String[]{ "NL","Netherlands","��������"};
		} else if (country.equalsIgnoreCase("Germany")) {
			abbreviation =new String[]{"DE","Germany","��������"};
		} else if (country.equalsIgnoreCase("Timor Leste") || country.equalsIgnoreCase("Timor-Leste")) {
			abbreviation =new String[]{"TL","Timor-Leste","��������� �����"};
		} else if (country.equalsIgnoreCase("Austria")) {
			abbreviation =new String[]{"AT","Austria","�������"};
		} else if ( country.equalsIgnoreCase("United States") ) {
			abbreviation =new String[]{"US","United States","�������� ��������� ��������"};
		}  else if ( country.equalsIgnoreCase("New Zealand") ) {
			abbreviation =new String[]{"NZ","New Zealand","��� ��������"};
		} else if (country.equalsIgnoreCase("France")) {
			abbreviation =new String[]{"FR","France","������"};
		} else if (country.equalsIgnoreCase("Italy")) {
			abbreviation =new String[]{"IT","Italy","������"};
		} else if (country.equalsIgnoreCase("Canada")) {
			abbreviation =new String[]{"CA","Canada","�������"};
		} else if (country.equalsIgnoreCase("China")) {
			abbreviation =new String[]{"CN","China","����"};
		} else if (country.equalsIgnoreCase("United Kingdom")) {
			abbreviation =new String[]{"GB","United Kingdom","������� ��������"};
		} else if (country.equalsIgnoreCase("Turkey")) {
			abbreviation =new String[]{"TR","Turkey","�������"};
		} else if (country.equalsIgnoreCase("Japan")) {
			abbreviation =new String[]{"JP","Japan","�������"};
		} else if (country.equalsIgnoreCase("United Arab Emirates")) {
			abbreviation =new String[]{"AE","United Arab Emirates","������� ������� �������"};
		} else if (country.equalsIgnoreCase("Switzerland")) {
			abbreviation =new String[]{"CH","Switzerland","�������"};
		} else if (country.equalsIgnoreCase("Spain")) {
			abbreviation =new String[]{"ES","Spain","�������"};
		} else if (country.equalsIgnoreCase("Belgium")) {
			abbreviation =new String[]{"BE","Belgium","������"};
		} else if (country.equalsIgnoreCase("Indonesia")) {
			abbreviation =new String[]{"ID","Indonesia","���������"};
		} else if (country.equalsIgnoreCase("Finland")) {
			abbreviation =new String[]{"FI","Finland","���������"};
		} else if (country.equalsIgnoreCase("Denmark")) {
			abbreviation =new String[]{"DK","Denmark","�����"};
		} else if (country.equalsIgnoreCase("Marshall Islands")) {
			abbreviation =new String[]{"MH","Marshall Islands","N���� ������"};
		} else if (country.equalsIgnoreCase("Ethiopia")) {
			abbreviation =new String[]{"ET","Ethiopia","��������"};
		} else if (country.equalsIgnoreCase("European Community")) {
			abbreviation =new String[]{"EU","European Community","��������� ���������"};
		} else if (country.equalsIgnoreCase("Australia")) {
				abbreviation =new String[]{"AU","Australia","���������"};
		} else if (country.equalsIgnoreCase("Bahrain")) {
			abbreviation =new String[]{"BH","Bahrain","��������"};
		} else if (country.equalsIgnoreCase("Brazil")) {
			abbreviation =new String[]{"BR","Brazil","��������"};
		} else if (country.equalsIgnoreCase("Cocos (Keeling) Islands")) {
			abbreviation =new String[]{"CC","Cocos (Keeling) Islands","����� ����� (�������)"};
		} else if (country.equalsIgnoreCase("Chile")) {
			abbreviation =new String[]{"CL","Chile","����"};
		} else if (country.equalsIgnoreCase("Cook Islands")) {
			abbreviation =new String[]{"CK","Cook Islands","N���� ����"};
		} else if (country.equalsIgnoreCase("Colombia")) {
			abbreviation =new String[]{"CO","Colombia","��������"};
		} else if (country.equalsIgnoreCase("Czech Republic")) {
			abbreviation =new String[]{"CZ","Czech Republic","������� ����������"};
		} else if (country.equalsIgnoreCase("Liberia")) {
			abbreviation =new String[]{"LR","Liberia","�������"};
		} else if (country.equalsIgnoreCase("Brunei Darussalam")) {
			abbreviation =new String[]{"BN","Brunei Darussalam","�������� �����������"};
		} else if (country.equalsIgnoreCase("Serbia")) {
			abbreviation =new String[]{"RS","Serbia","������"};
		} else if (country.equalsIgnoreCase("Jamaica")) {
			abbreviation =new String[]{"JM","Jamaica","��������"};
		} else if (country.equalsIgnoreCase("Swaziland")) {
			abbreviation =new String[]{"SZ","Swaziland","�����������"};
		} else if (country.equalsIgnoreCase("Barbados")) {
			abbreviation =new String[]{"��","Barbados","�����������"};
		} else if (country.equalsIgnoreCase("Egypt")) {
			abbreviation =new String[]{"EG","Egypt","��������"};
		} else if (country.equalsIgnoreCase("Seychelles")) {
			abbreviation =new String[]{"SC","Seychelles","���������"};
		} else if (country.equalsIgnoreCase("Isle of Man")) {
			abbreviation =new String[]{"IM","Isle of Man","����� ��� ���"};
		} else if (country.equalsIgnoreCase("Lithuania")) {
			abbreviation =new String[]{"LT","Lithuania","���������"};
		} else if (country.equalsIgnoreCase("Palau")) {
			abbreviation =new String[]{"PW","Palau","������"};
		} else if (country.equalsIgnoreCase("Malta")) {
			abbreviation =new String[]{"MT","Malta","�����"};
		} else if (country.equalsIgnoreCase("United States Minor Outlying Islands")) {
			abbreviation =new String[]{"UM","United States Minor Outlying Islands","�������������� ������� ��� �������� ���������"};
		} else if (country.equalsIgnoreCase("Belize")) {
			abbreviation =new String[]{"BZ","Belize","������"};
		} else if (country.equalsIgnoreCase("Maldives")) {
			abbreviation =new String[]{"MV","Maldives","��������"};
		} else if (country.equalsIgnoreCase("Jersey")) {
			abbreviation =new String[]{"JE","Jersey","Z�����"};
		} else if (country.equalsIgnoreCase("Tuvalu")) {
			abbreviation =new String[]{"TV","Tuvalu","��������"};
		} else if (country.equalsIgnoreCase("Mali")) {
			abbreviation =new String[]{"ML","Mali","����"};
		} else if (country.equalsIgnoreCase("Mozambique")) {
			abbreviation =new String[]{"MZ","Mozambique","���������"};
		} else if (country.equalsIgnoreCase("Syrian Arab Republic")) {
			abbreviation =new String[]{"SY","Syrian Arab Republic","������� ���������� ��� ������"};
		} else if (country.equalsIgnoreCase("Cayman Islands")) {
			abbreviation =new String[]{"KY","Cayman Islands","����� ������"};						
		} else if (country.equalsIgnoreCase("Guernsey")) {
			abbreviation =new String[]{"GG","Guernsey","��������"};
		} else if (country.equalsIgnoreCase("Iran, Islamic Republic of")) {
			abbreviation =new String[]{"IR","Islamic Republic of Iran","�������� ���������� ��� ����"};
		} else if (country.equalsIgnoreCase("Bosnia and Herzegovina")) {
			abbreviation =new String[]{"BA","Bosnia and Herzegovina","������ ��� ����������"};
		} else if (country.equalsIgnoreCase("Korea, Democratic People's Republic of")) {
			abbreviation =new String[]{"KP","Bosnia and Herzegovina","����� ���������� ��� �����"};
		} else if (country.equalsIgnoreCase("Togo")) {
			abbreviation =new String[]{"�G","Togo","������"};
		} else if (country.equalsIgnoreCase("Sri Lanka")) {
			abbreviation =new String[]{"LK","Sri Lanka","��� �����"};
		} else if (country.equalsIgnoreCase("Hong Kong")) {
			abbreviation =new String[]{"HK","Hong Kong","����� �����"};
		} else if (country.equalsIgnoreCase("Afghanistan")) {
			abbreviation =new String[]{"AF","Afghanistan","����������"};
		} else if (country.equalsIgnoreCase("Hungary")) {
			abbreviation =new String[]{"HU","Hungary","��������"};
		} else if (country.equalsIgnoreCase("Bangladesh")) {
			abbreviation =new String[]{"BD","Bangladesh","�����������"};
		} else if (country.equalsIgnoreCase("Romania")) {
			abbreviation =new String[]{"RO","Romania","��������"};
		} else if (country.equalsIgnoreCase("Libyan Arab Jamahiriya")) {
			abbreviation =new String[]{"LY","Libyan Arab Jamahiriya","������ ������� ����������"};
		} else if (country.equalsIgnoreCase("Kenya")) {
			abbreviation =new String[]{"KE","Kenya","�����"};
		} else if (country.equalsIgnoreCase("Iraq")) {
			abbreviation =new String[]{"IQ","Iraq","����"};
		} else if (country.equalsIgnoreCase("Lao People's Democratic Republic")) {
			abbreviation =new String[]{"LA","Lao People's Democratic Republic","����� ���������� ��� ����"};
		} else if (country.equalsIgnoreCase("Ireland")) {
			abbreviation =new String[]{"IE","Ireland","��������"};
		} else if (country.equalsIgnoreCase("Fiji")) {
			abbreviation =new String[]{"FJ","Fiji","�����"};
		} else if (country.equalsIgnoreCase("Christmas Island")) {
			abbreviation =new String[]{"CX","Christmas Island","����� ��� �������������"};
		} else if (country.equalsIgnoreCase("Israel")) {
			abbreviation =new String[]{"IL","Israel","������"};
		} else if (country.equalsIgnoreCase("India")) {
			abbreviation =new String[]{"IN","India","�����"};
		} else if (country.equalsIgnoreCase("Viet Nam")) {
			abbreviation =new String[]{"VN","Viet Nam","�������"};
		} else if (country.equalsIgnoreCase("Solomon Islands")) {
			abbreviation =new String[]{"SB","Solomon Islands","����� ��� ���������"};
		} else if (country.equalsIgnoreCase("Jordan")) {
			abbreviation =new String[]{"JO","Jordan","��������"};
		} else if (country.equalsIgnoreCase("Guam")) {
			abbreviation =new String[]{"GU","Guam","������"};
		} else if (country.equalsIgnoreCase("South Korea")) {
			abbreviation =new String[]{"KR","South Korea","����� �����"};
		} else if (country.equalsIgnoreCase("Kuwait")) {
			abbreviation =new String[]{"KW","Kuwait","�������"};
		} else if (country.equalsIgnoreCase("Uganda")) {
			abbreviation =new String[]{"UG","Uganda","��������"};
		} else if (country.equalsIgnoreCase("Croatia")) {
			abbreviation =new String[]{"HR","Croatia","�������"};
		} else if (country.equalsIgnoreCase("Kazakhstan")) {
			abbreviation =new String[]{"KZ","Kazakhstan","���������"};
		} else if (country.equalsIgnoreCase("Tonga")) {
			abbreviation =new String[]{"TO","Tonga","������"};
		} else if (country.equalsIgnoreCase("Lebanon")) {
			abbreviation =new String[]{"LB","Lebanon","������"};
		} else if (country.equalsIgnoreCase("Cambodia")) {
			abbreviation =new String[]{"KH","Cambodia","��������"};
		} else if (country.equalsIgnoreCase("Niue")) {
			abbreviation =new String[]{"NU","Niue","�����"};
		} else if (country.equalsIgnoreCase("Nepal")) {
			abbreviation =new String[]{"NP","Nepal","�����"};
		} else if (country.equalsIgnoreCase("Samoa")) {
			abbreviation =new String[]{"WS","Samoa","�����"};
		} else if (country.equalsIgnoreCase("New Caledonia")) {
			abbreviation =new String[]{"NC","New Caledonia","��� ���������"};
		} else if (country.equalsIgnoreCase("Slovakia")) {
			abbreviation =new String[]{"SK","Slovakia","��������"};
		} else if (country.equalsIgnoreCase("Greece")) {
			abbreviation =new String[]{"EL","Greece","������"};
		} else if (country.equalsIgnoreCase("Luxembourg")) {
			abbreviation =new String[]{"LU","Luxembourg","������������"};
		} else if (country.equalsIgnoreCase("Mauritius")) {
			abbreviation =new String[]{"MU","Mauritius","���������"};
		} else if (country.equalsIgnoreCase("Zimbabwe")) {
			abbreviation =new String[]{"ZW","Zimbabwe","����������"};
		} else if (country.equalsIgnoreCase("Mexico")) {
			abbreviation =new String[]{"MX","Mexico","������"};
		} else if (country.equalsIgnoreCase("Vanuatu")) {
			abbreviation =new String[]{"VU","Vanuatu","���������"};
		} else if (country.equalsIgnoreCase("Malaysia")) {
			abbreviation =new String[]{"MY","Malaysia","��������"};
		} else if (country.equalsIgnoreCase("Nigeria")) {
			abbreviation =new String[]{"NG","Nigeria","�������"};
		} else if (country.equalsIgnoreCase("Norway")) {
			abbreviation =new String[]{"NO","Norway","��������"};
		} else if (country.equalsIgnoreCase("Oman")) {
			abbreviation =new String[]{"OM","Oman","����"};
		} else if (country.equalsIgnoreCase("Kiribati")) {
			abbreviation =new String[]{"KI","Kiribati","���������"};
		} else if (country.equalsIgnoreCase("Botswana")) {
			abbreviation =new String[]{"BW","Botswana","����������"};
		} else if (country.equalsIgnoreCase("Peru")) {
			abbreviation =new String[]{"PE","Peru","�����"};
		} else if (country.equalsIgnoreCase("Philippines")) {
			abbreviation =new String[]{"PH","Philippines","����������"};
		} else if (country.equalsIgnoreCase("Pakistan")) {
			abbreviation =new String[]{"PK","Pakistan","��������"};
		} else if (country.equalsIgnoreCase("Zambia")) {
			abbreviation =new String[]{"ZM","Zambia","������"};
		} else if (country.equalsIgnoreCase("Argentina")) {
			abbreviation =new String[]{"AR","Argentina","���������"};
		} else if (country.equalsIgnoreCase("Micronesia, Federated States of")) {
			abbreviation =new String[]{"FM","Federated States of Micronesia","���������� ��������� ��� �����������"};
		} else if (country.equalsIgnoreCase("Poland")) {
			abbreviation =new String[]{"PL","Poland","�������"};
		} else if (country.equalsIgnoreCase("Bermuda")) {
			abbreviation =new String[]{"BM","Bermuda","��������"};
		} else if (country.equalsIgnoreCase("United Arab Emirates") || country.equalsIgnoreCase("UAE")) {
			abbreviation =new String[]{"AE","United Arab Emirates","������� ������� �������"};
		} else if (country.equalsIgnoreCase("Netherlands")) {
			abbreviation =new String[]{"NL","Netherlands","��������"};
		} else if (country.equalsIgnoreCase("Switzerland")) {
			abbreviation =new String[]{"CH","Switzerland","�������"};
		} else if (country.equalsIgnoreCase("Papua New Guinea")) {
			abbreviation =new String[]{"PG","Papua New Guinea","������ ��� �������"};
		} else if (country.equalsIgnoreCase("Puerto Rico")) {
			abbreviation =new String[]{"PR","Puerto Rico","������� ����"};						
		} else if (country.equalsIgnoreCase("Portugal")) {
			abbreviation =new String[]{"PT","Portugal","����������"};
		} else if (country.equalsIgnoreCase("Myanmar")) {
			abbreviation =new String[]{"MM","Myanmar","�������"};
		} else if (country.equalsIgnoreCase("Qatar")) {
			abbreviation =new String[]{"QA","Qatar","�����"};
		} else if (country.equalsIgnoreCase("Slovenia")) {
			abbreviation =new String[]{"SI","Slovenia","��������"};
		} else if (country.equalsIgnoreCase("Russia") || country.equalsIgnoreCase("Russian Federation")) {
			abbreviation =new String[]{"RU","Russian Federation","������ ����������"};
		} else if (country.equalsIgnoreCase("Guinea")) {
			abbreviation =new String[]{"GN","Guinea","�������"};
		} else if (country.equalsIgnoreCase("Monaco")) {
			abbreviation =new String[]{"MC","Monaco","������"};
		} else if (country.equalsIgnoreCase("Mongolia")) {
			abbreviation =new String[]{"MN","Mongolia","��������"};
		} else if (country.equalsIgnoreCase("Saudi Arabia")) {
			abbreviation =new String[]{"SA","Saudi Arabia","�������� ������"};
		} else if (country.equalsIgnoreCase("Sweden")) {
			abbreviation =new String[]{"SE","Sweden","�������"};
		} else if (country.equalsIgnoreCase("Norfolk Island")) {
			abbreviation =new String[]{"NF","Norfolk Island","����� �������"};
		} else if (country.equalsIgnoreCase("Singapore")) {
			abbreviation =new String[]{"SG","Singapore","����������"};
		} else if (country.equalsIgnoreCase("Indonesia")) {
			abbreviation =new String[]{"ID","Indonesia","���������"};
		} else if (country.equalsIgnoreCase("Thailand")) {
			abbreviation =new String[]{"TH","Thailand","��������"};
		} else if (country.equalsIgnoreCase("Nauru")) {
			abbreviation =new String[]{"NR","Nauru","�������"};
		} else if (country.equalsIgnoreCase("Bulgaria")) {
			abbreviation =new String[]{"BG","Bulgaria","���������"};
		} else if (country.equalsIgnoreCase("Panama")) {
			abbreviation =new String[]{"PA","Panama","�������"};
		} else if (country.equalsIgnoreCase("Dominica")) {
			abbreviation =new String[]{"DM","Dominica","���������"};
		} else if (country.equalsIgnoreCase("Taiwan")) {
			abbreviation =new String[]{"TW","Taiwan","������"};
		} else if (country.equalsIgnoreCase("Gibraltar")) {
			abbreviation =new String[]{"GI","Gibraltar","���������"};
		} else if (country.equalsIgnoreCase("Bolivia")) {
			abbreviation =new String[]{"BO","Bolivia","�������"};
		} else if (country.equalsIgnoreCase("Ukraine")) {
			abbreviation =new String[]{"UA","Ukraine","��������"};
		} else if (country.equalsIgnoreCase("Algeria")) {
			abbreviation =new String[]{"DZ","Algeria","�������"};
		} else if (country.equalsIgnoreCase("Iceland")) {
			abbreviation =new String[]{"IS","Iceland","��������"};
		} else if (country.equalsIgnoreCase("Grenada")) {
			abbreviation =new String[]{"GD","Grenada","�������"};
		} else if (country.equalsIgnoreCase("Republic of Korea") || country.equalsIgnoreCase("Korea, Republic of")) {
			abbreviation =new String[]{"KR","Republic of Korea","���������� ��� ������"};
		} else if (country.equalsIgnoreCase("Venezuela")) {
			abbreviation =new String[]{"VE","Venezuela","����������"};
		} else if (country.equalsIgnoreCase("Vietnam")) {
			abbreviation =new String[]{"VN","Vietnam","�������"};
		} else if (country.equalsIgnoreCase("French Polynesia")) {
			abbreviation =new String[]{"PF","French Polynesia","������� ���������"};
		} else if (country.equalsIgnoreCase("Morocco")) {
			abbreviation =new String[]{"MA","Morocco","������"};						
		} else if (country.equalsIgnoreCase("Bahamas")) {
			abbreviation =new String[]{"BS","Bahamas","��������"};						
		} else if (country.equalsIgnoreCase("Burundi")) {
			abbreviation =new String[]{"BI","Burundi","����������"};						
		} else if (country.equalsIgnoreCase("Benin")) {
			abbreviation =new String[]{"BJ","Benin","������"};						
		} else if (country.equalsIgnoreCase("South Africa")) {
			abbreviation =new String[]{"ZA","South Africa","����� ������"};
		} else if (country.equalsIgnoreCase("Ghana")) {
			abbreviation =new String[]{"GH","Ghana","�����"};
		} else {
			hm.writeUnknownMetadata("countries",country);
		}
		
		return abbreviation;
	}
		
}
