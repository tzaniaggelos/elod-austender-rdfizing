package utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import actions.CsvActions;
import ontology.Ontology;

/**
 * 
 * @author A. Tzanis
 *
 */
public class HelperMethods {

	
	
	/**
     * Find the corresponding SKOS Concept, its weight, the English name and the Greek name of the provided type of the criterion.
     * 
     * @param String the type of the procurementMethod
     * @return String the corresponding URI of the SKOS Concept, its weight,English name and Greek name
     * of the provided type of the procurement Method
     */
	public String[] findProcurementMethodIndividual(String procurementMethod) {
		
		String[] procurementMethodDtls = null;
		
		if (procurementMethod.equalsIgnoreCase("Open tender")) {
			procurementMethodDtls = new String[]{Ontology.publicContractPrefix + "Open", "Open", "Ανοικτός Διαγωνισμός"};
		} else if (procurementMethod.equalsIgnoreCase("Limited tender")) {
			procurementMethodDtls = new String[]{Ontology.publicContractPrefix + "Restricted", "Restricted", "Κλειστός Διαγωνισμός"};
		} else if (procurementMethod.equalsIgnoreCase("Prequalified tender")) {
			procurementMethodDtls = new String[]{Ontology.publicContractPrefix + "Prequalified", "Prequalified", "Συμφωνίες Πλαισίου"};
		} else { 
			procurementMethodDtls = new String[]{Ontology.publicContractPrefix + "Unknown", "", ""};
			writeUnknownMetadata("procurementMethod", procurementMethod);
		}
		
		return procurementMethodDtls;
	}
	
	
	/**
	 * Convert a date from 14-Aug-13 to 2013-08-14
	 * @param String date
	 * @return String date
	 */
	public String dateConvert (String date){
		
		String dateParts[] = date.split("-");
		
		// Convert month
		if (dateParts[1].equalsIgnoreCase("Jan")){
			dateParts[1] = "01";
		}
		else if (dateParts[1].equalsIgnoreCase("Feb")){
			dateParts[1] = "02";
		}
		else if (dateParts[1].equalsIgnoreCase("Mar")){
			dateParts[1] = "03";
		}
		else if (dateParts[1].equalsIgnoreCase("Apr")){
			dateParts[1] = "04";
		}else if (dateParts[1].equalsIgnoreCase("May")){
			dateParts[1] = "05";
		}else if (dateParts[1].equalsIgnoreCase("Jun")){
			dateParts[1] = "06";
		}else if (dateParts[1].equalsIgnoreCase("Jul")){
			dateParts[1] = "07";
		}else if (dateParts[1].equalsIgnoreCase("Aug")){
			dateParts[1] = "08";
		}else if (dateParts[1].equalsIgnoreCase("Sep")){
			dateParts[1] = "09";
		}else if (dateParts[1].equalsIgnoreCase("Oct")){
			dateParts[1] = "10";
		}else if (dateParts[1].equalsIgnoreCase("Nov")){
			dateParts[1] = "11";
		}else if (dateParts[1].equalsIgnoreCase("Dec")){
			dateParts[1] = "12";
		}
		
		// Convert Year
		if (dateParts[2].equalsIgnoreCase("90") || dateParts[2].equalsIgnoreCase("91") || dateParts[2].equalsIgnoreCase("92") || dateParts[2].equalsIgnoreCase("93")
				|| dateParts[2].equalsIgnoreCase("94") || dateParts[2].equalsIgnoreCase("95") || dateParts[2].equalsIgnoreCase("96") || dateParts[2].equalsIgnoreCase("97")
				|| dateParts[2].equalsIgnoreCase("98") || dateParts[2].equalsIgnoreCase("99")){
			dateParts[2] = "19"+dateParts[2];
		} 
		else {
			dateParts[2] = "20"+dateParts[2];
		}
		
		date = dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
		
		return date.replaceAll("\\s+","");
	}
	
	
	
	/**
	 * Convert a date from 14/8/2013 to 2013-08-14
	 * @param String date
	 * @return String date
	 */
	public String dateSemiConvert (String date){
		
		String dateParts[] = date.split("/");
		
		date = dateParts[2] + "-" + dateParts[1] + "-" + dateParts[0];
		
		return date.replaceAll("\\s+","");
	}
	
	
	
	/**
     * Find the filenames of the directories containing the CSV files.
     * 
     * @param String the path of the directory where the directories exist
     * @return List<String> the filenames of the directories containing the CSV files
     */
	public List<String> getDirectoryNames(String filePath) {
		
		List<String> namesList = new ArrayList<String>();
		File[] files = new File(filePath).listFiles();

		for (File file : files) {
		    if (file.isDirectory()) {
		        namesList.add(file.getName());
		    }
		}
		
		return namesList;
	}
	
	
	
	/**
     * Read the data from the text file and return them in a list.
     * 
     * @return List<String> the data of the text file
     */
	public List<String> readProcessedCsvFiles() {
		
		List<String> processedCsvList = new ArrayList<String>();
		
		try {
			BufferedReader in = new BufferedReader(new FileReader(CsvActions.filePathMain + "processedCsvFilenames.txt"));
			
			String line;
			while ((line = in.readLine()) != null) {
				processedCsvList.add(line);
			}
			
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Cannot read processed CSV file!");
		}

		return processedCsvList;
	}
	
	
	/**
     * Find the filenames of the CSV of the provided directory.
     * 
     * @param String the path of the directory where the files exist
     * @return List<String> the filenames of the CSV
     */
	public List<String> getCsvFileNames(String filePath) {
		
		List<String> namesList = new ArrayList<String>();
		File[] files = new File(filePath).listFiles();

		for (File file : files) {
		    if (file.isFile()) {
		        namesList.add(file.getName());
		    }
		}
		
		return namesList;
	}
	
	
	/**
     * Move the processed CSV files to a new directory.
     * 
     * @param String the directory name of the CSV file
     * @param String the date the process was initiated 
     * @param String the name of the CSV file
     */
	public void moveProcessedCsvFiles(String directoryName, String processDate, String csvFileName) {
		
		File csvFile = new File(CsvActions.filePathCsv + directoryName + "/" + csvFileName);
		Path from = csvFile.toPath();
		File toFile = directoryCreator (CsvActions.filePathCsvProcessed, directoryName + "/" + processDate);		// Create AE_pdf directory
		Path to = Paths.get(toFile + "/" + csvFile.getName());
		writeUnknownMetadata("processedCsvFilenames", csvFileName);													// Write the processed folders
		try {
			Files.move(from, to, StandardCopyOption.REPLACE_EXISTING);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * find the number of lines of a given csv file
	 * @param csvFileName
	 * @return linenumber
	 */
	public int numberOfCsvLines(String csvFileName){
		
		int linenumber = 0;
	    
		// find the number of lines
		try{
    		
    		File file =new File(csvFileName);
    		
    		if(file.exists()){
    			
    		    FileReader fr = new FileReader(file);
    		    LineNumberReader lnr = new LineNumberReader(fr);
    		    
    	            while (lnr.readLine() != null){
    	        	linenumber++;
    	            }
    	 
    	            lnr.close();
    	        
    		}else{
    			 System.out.println("File does not exists!");
    		}
    		
    	}catch(IOException e){
    		e.printStackTrace();
    	}
		
		return linenumber;
	}
	
	
	/**
	 *  Create a new directory.
	 *  
	 *  @param String the directory Path in which we want to create new directory
	 *  @param String the directory Name we want the created directory to have
	 */
    public static File directoryCreator (String directoryPath, String directoryName) {
       
    	File newDirectory = new File(directoryPath, directoryName);						// Create a new directory
		if(!newDirectory.exists()){
			newDirectory.mkdirs();
		}
		
        return newDirectory;
    }
  	
	
	/**
     * Find and transform the current date into the dd-MM-yyyy format.
     * 
     * @return String the current date in the dd-MM-yyyy format
     */
	public String getCurrentDate() {
		
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = sdf.format(cal.getTime());
		
		return currentDate;
	}
	
	
	/**
     * Export to a file the unknown metadata.
     * 
     * @param String the output filename
     * @param String the unknown metadata
     */
	public void writeUnknownMetadata(String fileName, String metadata) {
		
		try {
		    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(fileName + ".txt", true)));
		    out.println(metadata);
		    out.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
		
	}
	
	
}
