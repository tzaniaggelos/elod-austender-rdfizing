package actions;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import com.hp.hpl.jena.datatypes.xsd.XSDDatatype;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Resource;

import objects.Csv;
import ontology.Ontology;
import ontology.OntologyInitialization;
import utils.CountryOriented;
import utils.HelperMethods;
import utils.SupervisorOriented;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;

/**
 * 
 * @author A. Tzanis
 *
 */
public class RdfActions {

	/**
     * Fetch all the details from the provided Csv object, 
     * create the RDF triples and add them to the existing model.
     * 
     * @param csvObject a csv file object
     * @param Model the model to add the triples
     */
	public void createRdfFromCsv(Csv csvObject, Model model) {
		
		HelperMethods hm = new HelperMethods();
		SupervisorOriented bo = new SupervisorOriented();
		CountryOriented co = new CountryOriented();
		
		/** currencyResource **/
		Resource currencyResource = model.createResource(Ontology.instancePrefix + "Currency/AUD", Ontology.currencyResource);
		model.createResource(Ontology.instancePrefix + "Currency/AUD", Ontology.conceptsResource);
		/** configure prefLabel **/
		currencyResource.addProperty(Ontology.prefLabel, "Australian Dollar", "en");
		currencyResource.addProperty(Ontology.prefLabel, "Δολάριο Αυστραλίας", "el");
		
		/** supervisorResource **/
		Resource supervisorResource = null;
		if (csvObject.getBuyerName()!= null && !csvObject.getBuyerName().isEmpty()) {
			String[] supervisorCode = bo.findSupervisorCode(csvObject.getBuyerName());
			if (supervisorCode != null){
				supervisorResource = model.createResource(Ontology.instancePrefix + "Organization/" + supervisorCode[0].replaceAll("\\s+",""), Ontology.organizationResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + supervisorCode[0].replace("\\s+",""), Ontology.businessEntityResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + supervisorCode[0].replaceAll("\\s+",""), Ontology.orgOrganizationResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + supervisorCode[0].replaceAll("\\s+",""), Ontology.registeredOrganizationResource);
			}
			else {
				String buyerName = csvObject.getBuyerName().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
				supervisorResource = model.createResource(Ontology.instancePrefix + "Organization/" + buyerName, Ontology.organizationResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + buyerName, Ontology.businessEntityResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + buyerName, Ontology.orgOrganizationResource);
				model.createResource(Ontology.instancePrefix + "Organization/" + buyerName, Ontology.registeredOrganizationResource);
			}
			// supervisor legalName
			if (supervisorCode != null){
				supervisorResource.addLiteral(Ontology.legalName, model.createTypedLiteral(supervisorCode[1], XSDDatatype.XSDstring));		
			}
			else{
				supervisorResource.addLiteral(Ontology.legalName, model.createTypedLiteral(csvObject.getBuyerName().toUpperCase(), XSDDatatype.XSDstring));
			}
		}
		
		/** agencyResource **/
		Resource agencyResource = null;
		if (csvObject.getBuyerRefId()!= null && !csvObject.getBuyerRefId().isEmpty()) {
			String buyerRefId = csvObject.getBuyerRefId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			agencyResource = model.createResource(Ontology.instancePrefix + "Organization/" + buyerRefId, Ontology.organizationResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + buyerRefId, Ontology.businessEntityResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + buyerRefId, Ontology.orgOrganizationResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + buyerRefId, Ontology.registeredOrganizationResource);
			// agency hasSupervisorOrganization
			if (supervisorResource != null){
				agencyResource.addProperty(Ontology.hasSupervisorOrganization, supervisorResource);
			}
			// agency organizationId
			if (csvObject.getBuyerRefId() != null && !csvObject.getBuyerRefId().isEmpty()) {										
				agencyResource.addLiteral(Ontology.organizationId, model.createTypedLiteral(csvObject.getBuyerRefId().toUpperCase(), XSDDatatype.XSDstring));		
			}
			// agency division
			if (csvObject.getBuyerDivision() != null && !csvObject.getBuyerDivision().isEmpty()) {										
				agencyResource.addLiteral(Ontology.division, model.createTypedLiteral(csvObject.getBuyerDivision().toUpperCase(), XSDDatatype.XSDstring));		
			}
			// agency branch
			if (csvObject.getBuyerBranch() != null && !csvObject.getBuyerBranch().isEmpty()) {										
				agencyResource.addLiteral(Ontology.branch, model.createTypedLiteral(csvObject.getBuyerBranch().toUpperCase(), XSDDatatype.XSDstring));		
			}
			/** addressResource **/
			Resource addressResource = null;
			if (csvObject.getBuyerRefId()!= null && !csvObject.getBuyerRefId().isEmpty()){
				addressResource = model.createResource(Ontology.instancePrefix + "Address/" + buyerRefId, Ontology.addressResource);
			}
			// address countryName
			if (csvObject.getBuyerCountry() != null && !csvObject.getBuyerCountry().isEmpty()) {										
				String[] countryIsoCode = co.findCountryAbbreviation(csvObject.getBuyerCountry());
				if (countryIsoCode[0] != null) {
					Resource countryResource = model.createResource(Ontology.instancePrefix + countryIsoCode[0], Ontology.countryResource);
					model.createResource(Ontology.instancePrefix + countryIsoCode[0], Ontology.conceptsResource);
					/** configure prefLabel **/
					countryResource.addProperty(Ontology.prefLabel, countryIsoCode[1], "en");
					countryResource.addProperty(Ontology.prefLabel, countryIsoCode[2], "el");
					
					addressResource.addProperty(Ontology.countryName, countryResource);		
				}
				else {
					String buyerCountry = csvObject.getBuyerCountry().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
					Resource countryResource = model.createResource(Ontology.instancePrefix + buyerCountry, Ontology.countryResource);
					addressResource.addProperty(Ontology.countryName, countryResource);
				}
			}
			// address postalCode
			if (csvObject.getBuyerPostcode() != null && !csvObject.getBuyerPostcode().isEmpty()) {										
				addressResource.addLiteral(Ontology.postalCode, model.createTypedLiteral(csvObject.getBuyerPostcode().toUpperCase(), XSDDatatype.XSDstring));		
			}
			
			// agency hasAddress
			if (addressResource != null){
				agencyResource.addProperty(Ontology.hasAddress, addressResource);
			}
		}
		
		// supervisor isSupervisorOf
		if (agencyResource != null){
			supervisorResource.addProperty(Ontology.isSupervisorOrganizationOf, agencyResource);
		}
		
		/** supplierResource **/
		Resource supplierResource = null;
		if (csvObject.getSellerAbn()!= null && !csvObject.getSellerAbn().isEmpty()) {
			String sellerABN = csvObject.getSellerAbn().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			supplierResource = model.createResource(Ontology.instancePrefix + "Organization/" + sellerABN, Ontology.organizationResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + sellerABN, Ontology.businessEntityResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + sellerABN, Ontology.orgOrganizationResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + sellerABN, Ontology.registeredOrganizationResource);
		}
		else if (csvObject.getSellerName()!= null && !csvObject.getSellerName().isEmpty()) {
			String sellerName = csvObject.getSellerName().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			supplierResource = model.createResource(Ontology.instancePrefix + "Organization/" + sellerName, Ontology.organizationResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + sellerName, Ontology.businessEntityResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + sellerName, Ontology.orgOrganizationResource);
			model.createResource(Ontology.instancePrefix + "Organization/" + sellerName, Ontology.registeredOrganizationResource);
		}
			// supplier Name
			if (csvObject.getSellerName() != null && !csvObject.getSellerName().isEmpty()){
				supplierResource.addLiteral(Ontology.name, model.createTypedLiteral(csvObject.getSellerName().toUpperCase(), XSDDatatype.XSDstring));		
			}
			// supplier vatID
			if (csvObject.getSellerAbn() != null && !csvObject.getSellerAbn().isEmpty()) {										
				supplierResource.addLiteral(Ontology.vatID, model.createTypedLiteral(csvObject.getSellerAbn().toUpperCase(), XSDDatatype.XSDstring));		
			}
			
			/** supplier addressResource **/
			Resource addressResource = null;
			if (csvObject.getSellerAbn()!= null && !csvObject.getSellerAbn().isEmpty()) {
				String sellerABN = csvObject.getSellerAbn().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
				addressResource = model.createResource(Ontology.instancePrefix + "Address/" + sellerABN, Ontology.addressResource);
			}
			else if (csvObject.getSellerName()!= null && !csvObject.getSellerName().isEmpty()) {
				String sellerName = csvObject.getSellerName().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
				addressResource = model.createResource(Ontology.instancePrefix + "Address/" + sellerName, Ontology.addressResource);
			}
			// supplier address countryName
			if (csvObject.getSellerCountry() != null && !csvObject.getSellerCountry().isEmpty()) {										
				String[] countryIsoCode = co.findCountryAbbreviation(csvObject.getSellerCountry());
				if (countryIsoCode[0] != null && !countryIsoCode[0].isEmpty()) {
					Resource countryResource = model.createResource(Ontology.instancePrefix + countryIsoCode[0].replaceAll("\\s+",""), Ontology.countryResource);
					model.createResource(Ontology.instancePrefix + countryIsoCode[0].replaceAll("\\s+",""), Ontology.conceptsResource);
					/** configure prefLabel **/
					countryResource.addProperty(Ontology.prefLabel, countryIsoCode[1], "en");
					countryResource.addProperty(Ontology.prefLabel, countryIsoCode[2], "el");
					
					addressResource.addProperty(Ontology.countryName, countryResource);		
				}
				else {
					String sellerCountry = csvObject.getSellerCountry().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
					Resource countryResource = model.createResource(Ontology.instancePrefix + sellerCountry, Ontology.countryResource);
					addressResource.addProperty(Ontology.countryName, countryResource);
				}
			}
			// address postalCode
			if (csvObject.getSellerPostcode() != null && !csvObject.getSellerPostcode().isEmpty()) {										
				addressResource.addLiteral(Ontology.postalCode, model.createTypedLiteral(csvObject.getSellerPostcode().toUpperCase(), XSDDatatype.XSDstring));		
			}
			// address streetAddress
			if (csvObject.getSellerAddress() != null && !csvObject.getSellerAddress().isEmpty()) {										
				addressResource.addLiteral(Ontology.streetAddress, model.createTypedLiteral(csvObject.getSellerAddress().toUpperCase(), XSDDatatype.XSDstring));		
			}
			// address locality
			if (csvObject.getSellerCity() != null && !csvObject.getSellerCity().isEmpty()) {										
				addressResource.addLiteral(Ontology.locality, model.createTypedLiteral(csvObject.getSellerCity().toUpperCase(), XSDDatatype.XSDstring));		
			}
						
			// supplier hasAddress
			if (addressResource != null ){
				supplierResource.addProperty(Ontology.hasAddress, addressResource);
			}
				
		/** unitPriceSpecificationResource **/
		Resource unitPriceSpecificationResource = null;
		if (csvObject.getContractId() != null && !csvObject.getContractId().isEmpty()){
			String contractId = csvObject.getContractId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			unitPriceSpecificationResource = model.createResource(Ontology.instancePrefix + "UnitPriceSpecification/" + contractId, Ontology.unitPriceSpecificationResource);
			// unitPriceSpecification hasCurrencyValue
			if (csvObject.getValue() != 0.0){
				unitPriceSpecificationResource.addLiteral(Ontology.hasCurrencyValue, model.createTypedLiteral(csvObject.getValue(), XSDDatatype.XSDfloat));
			}
			// unitPriceSpecification valueAddedTaxIncluded
			unitPriceSpecificationResource.addLiteral(Ontology.valueAddedTaxIncluded, model.createTypedLiteral("1", XSDDatatype.XSDstring));		
			// unitPriceSpecification hasCurrency
			if (currencyResource != null){
				unitPriceSpecificationResource.addProperty(Ontology.hasCurrency, currencyResource);
			}
		}
		
		/** expenditureLineResource **/
		Resource expenditureLineResource = null;
		if (csvObject.getContractId() != null && !csvObject.getContractId().isEmpty()){
			String contractId = csvObject.getContractId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			expenditureLineResource = model.createResource(Ontology.instancePrefix + "ExpenditureLine/" + contractId, Ontology.expenditureLineResource);
			// expenditureLine amount	
			if (unitPriceSpecificationResource != null){
				expenditureLineResource.addProperty(Ontology.amount, unitPriceSpecificationResource);
			}
			// expenditureLine seller
			if (supplierResource != null){
				expenditureLineResource.addProperty(Ontology.seller, supplierResource);
			}
		}
		
		/** unspscCategoryResource **/
		Resource unspscCategoryResource = null;
		if (csvObject.getUnspscCode() != null && !csvObject.getUnspscCode().isEmpty()){
			String unspscCode = csvObject.getUnspscCode().replaceAll("\\s+","");
			unspscCategoryResource = model.createResource(Ontology.instancePrefix + "UNSPSC/" + unspscCode, Ontology.unspscCategoryResource);
			// unspscCategory unspscSubject
			if (csvObject.getUnspscSubject() != null && !csvObject.getUnspscSubject().isEmpty()){
				unspscCategoryResource.addLiteral(Ontology.unspscSubject, model.createTypedLiteral(csvObject.getUnspscSubject().toUpperCase(), XSDDatatype.XSDstring));
			}
			// unspscCategory unspscCode
			unspscCategoryResource.addLiteral(Ontology.unspscCode, model.createTypedLiteral(csvObject.getUnspscCode().toUpperCase(), XSDDatatype.XSDstring));
		}
		
		/** someItemsResource **/
		Resource someItemsResource = null;
		if (csvObject.getContractId() != null && !csvObject.getContractId().isEmpty()){
			String contractId = csvObject.getContractId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			someItemsResource = model.createResource(Ontology.instancePrefix + "SomeItemsResource/" + contractId, Ontology.someItemsResource);
			// someItemsResource description
			if (csvObject.getDescription() != null && !csvObject.getDescription().isEmpty()){
				someItemsResource.addLiteral(Ontology.description, model.createTypedLiteral(csvObject.getDescription().toUpperCase(), XSDDatatype.XSDstring));		
			}
			// someItemsResource hasUNSPSC
			if (unspscCategoryResource != null){
				someItemsResource.addProperty(Ontology.hasUNSPSC, unspscCategoryResource);
			}
		}
		
		/** typeAndQuantityNodeResource **/
		Resource typeAndQuantityNodeResource = null;
		if (csvObject.getContractId() != null && !csvObject.getContractId().isEmpty()){
			String contractId = csvObject.getContractId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			typeAndQuantityNodeResource = model.createResource(Ontology.instancePrefix + "TypeAndQuantityNode/" + contractId, Ontology.typeAndQuantityNodeResource);
			// typeAndQuantityNode typeOfGood
			if (someItemsResource != null){
				typeAndQuantityNodeResource.addProperty(Ontology.typeOfGood, someItemsResource);
			}
		}
		
		/** offeringResource **/
		Resource offeringResource = null;
		if (csvObject.getContractId() != null && !csvObject.getContractId().isEmpty()){
			String contractId = csvObject.getContractId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			offeringResource = model.createResource(Ontology.instancePrefix + "Offering/" + contractId, Ontology.offeringResource);
			// offering includesObject
			if (typeAndQuantityNodeResource != null){
				offeringResource.addProperty(Ontology.includesObject, typeAndQuantityNodeResource);
			}
			// offering seller
			if (supplierResource != null){
				offeringResource.addProperty(Ontology.seller, supplierResource);
			}
		}
		
		/** procurementMethodResource **/
		Resource procurementMethodResource = null;
		if (csvObject.getProcurementMethod() != null && !csvObject.getProcurementMethod().isEmpty()){
			String[] procurementMethodIndividualUri = hm.findProcurementMethodIndividual(csvObject.getProcurementMethod());
			procurementMethodResource = model.createResource(procurementMethodIndividualUri[0], Ontology.conceptsResource);
		}
		
		/** personResource **/
		Resource personResource = null;
		if ((csvObject.getContractId() != null && !csvObject.getContractId().isEmpty()) && 
				(csvObject.getPhone() != null && !csvObject.getPhone().isEmpty() || csvObject.getPersonName() != null && !csvObject.getPersonName().isEmpty())){
			String contractId = csvObject.getContractId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			personResource = model.createResource(Ontology.instancePrefix + "Person/" + contractId, Ontology.personResource);
			// person phone
			if (csvObject.getPhone() != null && !csvObject.getPhone().isEmpty()){
				personResource.addLiteral(Ontology.personPhone, model.createTypedLiteral(csvObject.getPhone(), XSDDatatype.XSDstring));
			}
			// person name
			if (csvObject.getPersonName() != null && !csvObject.getPersonName().isEmpty()){
				personResource.addLiteral(Ontology.personName, model.createTypedLiteral(csvObject.getPersonName().toUpperCase(), XSDDatatype.XSDstring));
			}
		}
		
		/** contractResource **/
		Resource contractResource = null;
		if (csvObject.getContractId() != null && !csvObject.getContractId().isEmpty()){
			String contractId = csvObject.getContractId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			contractResource = model.createResource(Ontology.instancePrefix + "Contract/" + contractId, Ontology.contractResource);
			// contract seller
			if (supplierResource != null){
				contractResource.addProperty(Ontology.seller, supplierResource);
			}
			// contract buyer
			if (agencyResource != null) {
				contractResource.addProperty(Ontology.buyer, agencyResource);
			}
			// contract contractId
			contractResource.addLiteral(Ontology.contractId, model.createTypedLiteral(csvObject.getContractId().toUpperCase(), XSDDatatype.XSDstring));
			// contract actualPrice
			if (unitPriceSpecificationResource != null) {
				contractResource.addProperty(Ontology.actualPrice, unitPriceSpecificationResource);
			}
			// contract item
			if (offeringResource != null) {
				contractResource.addProperty(Ontology.item, offeringResource);
			}
			// contract procedureType
			if (procurementMethodResource != null){
				contractResource.addProperty(Ontology.procedureType, procurementMethodResource);
			}
			// contract startDate
			if (csvObject.getStartDate() != null && !csvObject.getStartDate().isEmpty()){
				contractResource.addLiteral(Ontology.startDate, model.createTypedLiteral(csvObject.getStartDate(), XSDDatatype.XSDdate));
			}
			// contract estimatedEndDate
			if (csvObject.getEndDate() != null && !csvObject.getEndDate().isEmpty()){
				contractResource.addLiteral(Ontology.estimatedEndDate, model.createTypedLiteral(csvObject.getEndDate(), XSDDatatype.XSDdate));
			}
			// contract contact
			if (personResource != null){
				contractResource.addProperty(Ontology.contact, personResource);
			}
			// contract issued
			if (csvObject.getPublishDate() != null && !csvObject.getPublishDate().isEmpty()){
				contractResource.addLiteral(Ontology.issued, model.createTypedLiteral(csvObject.getPublishDate(), XSDDatatype.XSDdate));
			}
		}
		
		/** spendingItemResource **/
		Resource spendingItemResource = null;
		if (csvObject.getContractId() != null && !csvObject.getContractId().isEmpty()){
			String contractId = csvObject.getContractId().replaceAll("[ -/]", "_").replaceAll("\\s+","").toUpperCase();
			spendingItemResource = model.createResource(Ontology.instancePrefix + "SpendingItem/" + contractId, Ontology.spendingItemResource);
			// spendingItem hasExpenditureLine
			if (expenditureLineResource != null){
				spendingItemResource.addProperty(Ontology.hasExpenditureLine, expenditureLineResource);
			}
			// spendingItem hasRelatedContract
			if (contractResource != null){
				spendingItemResource.addProperty(Ontology.hasRelatedContract, contractResource);
			}
			// spendingItem buyer
			if (agencyResource != null) {
				spendingItemResource.addProperty(Ontology.buyer, agencyResource);	
			}
		}
	}
	
	
	/**
     * Fetch the existing model depending whether it is located remotely or locally.
     * 
     * @param boolean is the model located remotely or locally?
     * @param Name the filename
     * @param Part the part of the file
     * @return Model the new model
     */
	public Model remoteOrLocalModel (boolean isRemote , String name , int part) {
		
		OntologyInitialization ontInit = new OntologyInitialization();
		
		Model remoteModel = ModelFactory.createDefaultModel();
		
		if (isRemote) {
			String graphName = "http://linkedeconomy.org/Australia";
			String connectionString = "jdbc:virtuoso://83.212.86.155:1111/autoReconnect=true/charset=UTF-8/log_enable=2";
			VirtGraph graph = new VirtGraph (graphName, connectionString, "razis", "m@kisr@zis");
			System.out.println("\nConnected to remote Australia graph!\n");
	    	remoteModel = new VirtModel(graph);
		} else {
			try {
				InputStream is = null;
				if (part == 0){
					is = new FileInputStream(CsvActions.filePathOutput + "/" + CsvActions.rdfName + name + ".rdf");
				}
				else {
					is = new FileInputStream(CsvActions.filePathOutput + "/" + CsvActions.rdfName + name + "_Part_" + part + ".rdf");
				}
				remoteModel.read(is,null);
				is.close();
			} catch (Exception e) { //empty file
			}
		}
		
		ontInit.setPrefixes(remoteModel);
		
		return remoteModel;
		
	}
	
	
	/**
     * Store the Model.
     * 
     * @param Model the model
     * @param Name the filename
     * @param Part the part of the file
     */
	public void writeModel(Model model, String name, int part) {
		
		try {
			System.out.println("\nSaving Model...");
			FileOutputStream fos = null;
			if (part==0){
				fos = new FileOutputStream(CsvActions.filePathOutput + "/" + CsvActions.rdfName + name + ".rdf");
				model.write(fos, "RDF/XML-ABBREV", CsvActions.filePathOutput + "/" + CsvActions.rdfName + name + ".rdf");
			}
			else{
				fos = new FileOutputStream(CsvActions.filePathOutput + "/" + CsvActions.rdfName + name + "_Part_" + part + ".rdf");
				model.write(fos, "RDF/XML-ABBREV", CsvActions.filePathOutput + "/" + CsvActions.rdfName + name + "_Part_" + part + ".rdf");
			}
			fos.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
}
