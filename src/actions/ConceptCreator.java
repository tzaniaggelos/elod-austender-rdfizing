package actions;

import java.util.Arrays;
import java.util.List;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;

import ontology.Ontology;
import utils.HelperMethods;

/**
 * 
 * @author A. Tzanis
 *
 */
public class ConceptCreator {

private HelperMethods hm = new HelperMethods();
	
	/**
     * Add to the model the procurement Methods.
     * 
     * @param Model the model we are currently working with
     */
	public void addProcurementMethods(Model model) {
		
		List<String> procurementMethodsList = Arrays.asList("Open Tender", "Limited Tender", "Prequalified Tender");
		
		for (String procurementMethod : procurementMethodsList) {
			String[] procurementMethodDtls = hm.findProcurementMethodIndividual(procurementMethod);
			
			/** procurementMethodResource **/
			Resource procurementMethodResource = model.createResource(procurementMethodDtls[0], Ontology.conceptsResource);
			//model.createResource(procurementMethodDtls[0], Ontology.conceptsResource);
			
			/** configure prefLabel **/
			procurementMethodResource.addProperty(Ontology.prefLabel, procurementMethodDtls[1], "en");
			procurementMethodResource.addProperty(Ontology.prefLabel, procurementMethodDtls[2], "el");
		
			/** configure concept properties **/
			procurementMethodResource.addProperty(Ontology.inScheme, Ontology.procedureTypeSchemeResource);
			procurementMethodResource.addProperty(Ontology.topConceptOf, Ontology.procedureTypeSchemeResource);
			
			/** Create conceptScheme Resource **/
			Resource conceptSchemeResource = model.createResource(Ontology.publicContractPrefix + "ProcedureTypeScheme", Ontology.conceptSchemeResource);
			
			/** configure conceptScheme property **/
			conceptSchemeResource.addProperty(Ontology.hasTopConcept, procurementMethodResource);
			
		}
		
	}
	
	
}
