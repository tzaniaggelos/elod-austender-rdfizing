package actions;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.hp.hpl.jena.rdf.model.Model;

import objects.Csv;
import ontology.OntologyInitialization;
import utils.HelperMethods;

/**
 * 
 * @author A. Tzanis
 *
 */
public class Main {

	public static void main(String[] args) throws UnsupportedEncodingException {
		
		boolean createConcepts = true;
		
		HelperMethods hm = new HelperMethods();
		CsvActions csvMethods = new CsvActions();
		RdfActions rdfActions = new RdfActions();
		ConceptCreator concepts = new ConceptCreator();
		OntologyInitialization ontInit = new OntologyInitialization();
		List<String> processedCsvList = new ArrayList<String>();			// List of processed Csv files
		processedCsvList = hm.readProcessedCsvFiles();						// Load the processed CSV files
		String processDate = null;											// String to store the process date
		processDate = hm.getCurrentDate();									// Current date
		int lineCount = 0;
		int line = 0;
		int part = 0;
		String[] fields = null;
		
		Model model = null;
		
		List<String> csvFileNameList = new ArrayList<String>();				// List of csv file names
		List<String> direcoryNameList = new ArrayList<String>();			// List of directory names
		direcoryNameList = hm.getDirectoryNames(CsvActions.filePathCsv);	// Stores the filePath directory direstory names to the list
		
		for (String directoryName : direcoryNameList) {
			
			if ( directoryName.equalsIgnoreCase("Csv") ){
				
				csvFileNameList = hm.getCsvFileNames(CsvActions.filePathCsv + directoryName);
				
				for (String csvFileName : csvFileNameList) {
					
					model = rdfActions.remoteOrLocalModel(false, csvFileName.replaceAll(".csv", ""), 0);
					
					//Perform the basic initialization on the model
					ontInit.setPrefixes(model);
					ontInit.createHierarchies(model);
					
					//add the Concepts
					if (createConcepts) {
						concepts.addProcurementMethods(model);
					}
					
					if (!processedCsvList.contains(csvFileName)) { //file not in list
						
						line=0;
						
						try {
							
							BufferedReader buf = new BufferedReader(new FileReader(CsvActions.filePathCsv + directoryName + "/" + csvFileName));				// In case directory is searchResultsFinal
							String nextLine;
							lineCount = 0;
							part = 0;
							
							try {
								
								System.out.println("\n" + CsvActions.filePathCsv + directoryName + "/" + csvFileName);
								lineCount = hm.numberOfCsvLines(CsvActions.filePathCsv + directoryName + "/" + csvFileName);
								
								while ((nextLine = buf.readLine()) != null) {
									
									line++;										// Increase the line number by one 
									
									if (!csvFileName.equalsIgnoreCase("AusTender CN Database 1Jul07 to 31Dec13.csv")){
										fields = nextLine.split("\t");		// Seperate the line by "tab"
										// Pass the first 3 lines
										if(line>3){
											// check if line is ok or it is currupted
											if(fields.length == 32){
												Csv csvObject = csvMethods.readCsv(fields,line);		// Creates the csv object
												rdfActions.createRdfFromCsv(csvObject, model);			// Stores the object to the model
											}
										}
									}
									// checks if csv is seperated by ";" or by "tab"
									else if (csvFileName.equalsIgnoreCase("AusTender CN Database 1Jul07 to 31Dec13.csv")){
										fields = nextLine.split(",");			// Seperate the line by ";"
										// Pass the first 3 lines
										if(line>3){
											// check if line is ok or it is currupted
											if(fields.length == 34){
												Csv csvObject = csvMethods.readCsv(fields,line);		// Creates the csv object
												rdfActions.createRdfFromCsv(csvObject, model);			// Stores the object to the model
											}
										}
									}
									
									
									
									// if the file is too large we divide the ref in parts every 100.000 csv lines
									if (lineCount > 120000 && line%100000 == 0){
										
										part++;
										// Saves the model and close it
										rdfActions.writeModel(model, csvFileName.replaceAll(".csv", ""), part);				// store the models at the end of each folder 
										model.close();																		// close current model
										
										// recreate the model
										model = rdfActions.remoteOrLocalModel(false, csvFileName.replaceAll(".csv", ""), part+1);
										
										//Perform the basic initialization on the model
										ontInit.setPrefixes(model);
										ontInit.createHierarchies(model);
										
										//add the Concepts
										if (createConcepts) {
											concepts.addProcurementMethods(model);
										}
									}
								}
								buf.close();	
							} catch (IOException e) {
								e.printStackTrace();
							}
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						}
						// if csv file has less than 120.000 lines
						if (part == 0 ){
							rdfActions.writeModel(model, csvFileName.replaceAll(".csv", ""), 0);				// store the models at the end of each folder 
						}
						// else if csv file has more than 120.000 lines
						else{
							rdfActions.writeModel(model, csvFileName.replaceAll(".csv", ""), part+1);			// store the models at the end of each folder
						}
						model.close();																			// close current model
						
						hm.moveProcessedCsvFiles(directoryName, processDate, csvFileName); 						// Move the file to Processed file
					}
				}
				hm.writeUnknownMetadata("foldersOK", directoryName);											// Write the processed folders
			}
			processedCsvList = hm.readProcessedCsvFiles();														// Load the processed CSV files at the and of each folder
		}
			System.out.println("\nFinished!");
	}
}
