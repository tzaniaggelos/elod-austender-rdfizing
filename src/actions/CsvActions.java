package actions;

import java.io.UnsupportedEncodingException;

import objects.Csv;
import utils.HelperMethods;
//import utils.SupervisorOriented;

/**
 * @author A. Tzani
 */
public class CsvActions {

	public static final String rdfName = "Australia_";
	
	/** local **/
	/*public static final String filePathCsv = "C:/Users/Aggelos/workspace/Australia/";							// Csv files filePath
	public static final String filePathOutput = "C:/Users/Aggelos/workspace/Australia/RdfFiles";				// Output filepath
	public static final String filePathMain = "C:/Users/Aggelos/workspace/Australia/";							// Main workspace filepath
	public static final String filePathCsvProcessed = "C:/Users/Aggelos/workspace/Australia/ProcessedCsv/"; 	// Processed csv files filePath
	*/
	
	/** server **/
	public static final String filePathOutput = "/home/tzanis/Australia/rdfizing/RdfFiles/17_3_2016";						// Output filepath server
	public static final String filePathMain = "/home/tzanis/Australia/rdfizing/";								// Main workspace filepath server
	public static final String filePathCsvProcessed = "/home/tzanis/Australia/rdfizing/ProcessedCsv/"; 			// Processed csv files filePath server
	public static final String filePathCsv = "/home/tzanis/Australia/rdfizing/";								// Csv files filePath server
	
	
	public Csv readCsv(String[] csvLine, int line) throws UnsupportedEncodingException {
			
		HelperMethods hm = new HelperMethods();
		//SupervisorOriented so = new SupervisorOriented();
		
		String buyerName = null; 							// Buyer name					i=0		i=0
		String contractId = null;							// Contract id					i=2		i=2
		String publishDate = null;							// Contract publish date		i=3		i=3
		String startDate = null;							// Contract start date			i=6		i=5
		String endDate = null;								// Contract end date			i=7		i=6
		double value = 0.00;								// Contract value				i=8		i=7
		String description = null;							// Contract description			i=9		i=8
		String buyerRefId = null;							// Buyer ref. id				i=10	i=9
		String category = null;								// Contract category			i=11
		String procurementMethod = null;					// Procurement method			i=12	i=12
		String sellerName = null;							// Seller name					i=22	i=22
		String sellerAddress = null;						// Seller address				i=23	i=23
		String sellerCity = null;							// Seller city					i=24	i=24
		String sellerPostcode = null;						// Seller post-code				i=25	i=25
		String sellerCountry = null;						// Seller country				i=26	i=26
		String sellerAbn = null;							// Seller ABN 					i=28	i=28
		String buyerBranch = null;							// Buyer branch					i=29	i=31
		String buyerDivision = null;						// Buyer division				i=30	i=32
		String buyerPostcode = null;						// Buyer post-code				i=31	i=33
		String unspscCode = null;							// Unspsc Code                			i=10		
		String unspscSubject = null;						// Unspsc Code                			i=11		
		String contactName = null;							// Contact name							i=29	
		String contactPhone = null;							// Contact phone						i=30	
		String buyerCountry = "AUSTRALIA";					// Buyer country
		
		Csv csvObject = null;
		
		if (csvLine.length == 32){
			if (!csvLine[0].isEmpty()){
				buyerName = csvLine[0].replaceAll("\"", "");
				System.out.print("buyerName = "+buyerName+"\t");
			} if (!csvLine[2].isEmpty()){
				contractId = csvLine[2].replaceAll("\"", "");
				System.out.print("contractId = "+contractId+"\t");
			} if (!csvLine[3].isEmpty()){
				publishDate = hm.dateConvert(csvLine[3].substring(0, 9));
				System.out.print("publishDate = "+publishDate+"\t");
			} if (!csvLine[6].isEmpty()){
				startDate = hm.dateConvert(csvLine[6].substring(0, 9));
				System.out.print("startDate = "+startDate+"\t");
			} if (!csvLine[7].isEmpty()){
				endDate = hm.dateConvert(csvLine[7].substring(0, 9));
				System.out.print("endDate = "+endDate+"\t");
			} if (!csvLine[8].isEmpty()){
				value = Double.parseDouble(csvLine[8]);
				System.out.print("value = "+value+"\t");
			} if (!csvLine[9].isEmpty()){
				description = csvLine[9].replaceAll("\"", "");
				System.out.print("description = "+description+"\t");
			} if (!csvLine[10].isEmpty()){
				buyerRefId = csvLine[10].replaceAll("\"", "");
				System.out.print("buyerRefId = "+buyerRefId+"\t");
			} if (!csvLine[11].isEmpty()){
				category = csvLine[11].replaceAll("\"", "");
				System.out.print("category = "+category+"\t");
			} if (csvLine[12] != null && !csvLine[12].isEmpty()){
				procurementMethod = csvLine[12].replaceAll("\"", "");
				System.out.print("procurementMethod = "+procurementMethod+"\t");
			}  if (!csvLine[22].isEmpty()){
				sellerName = csvLine[22].replaceAll("\"", "");
				System.out.print("sellerName = "+sellerName+"\t");
			} if (!csvLine[23].isEmpty()){
				sellerAddress = csvLine[23].replaceAll("\"", "");
				System.out.print("sellerAddress = "+sellerAddress+"\t");
			} if (!csvLine[24].isEmpty()){
				sellerCity = csvLine[24].replaceAll("\"", "");
				System.out.print("sellerCity = "+sellerCity+"\t");
			} if (!csvLine[25].isEmpty()){
				sellerPostcode = csvLine[25].replaceAll("\"", "");
				System.out.print("sellerPostcode = "+sellerPostcode+"\t");
			} if (!csvLine[26].isEmpty()){
				sellerCountry = csvLine[26].replaceAll("\"", "");
				System.out.print("sellerCountry = "+sellerCountry+"\t");
			} if (!csvLine[28].isEmpty()){
				sellerAbn = csvLine[28].replaceAll("\"", "");
				System.out.print("sellerAbn = "+sellerAbn+"\t");
			} if (!csvLine[29].isEmpty()){
				buyerBranch = csvLine[29].replaceAll("\"", "");
				System.out.print("buyerBranch = "+buyerBranch+"\t");
			} if (!csvLine[30].isEmpty()){
				buyerDivision = csvLine[30].replaceAll("\"", "");
				System.out.print("buyerDivision = "+buyerDivision+"\t");
			} if (!csvLine[31].isEmpty()){
				buyerPostcode = csvLine[31].replaceAll("\"", "");
				System.out.print("buyerPostcode = "+buyerPostcode+"\n");
			}
		}
		
		else if (csvLine.length == 34 && line != 11243) {
			if (!csvLine[0].isEmpty()){
				buyerName = csvLine[0].replaceAll("\"", "");
				//so.findSupervisorCode(buyerName,line);
				System.out.print("buyerName = "+buyerName+"\t");
			} if (!csvLine[2].isEmpty()){
				contractId = csvLine[2].replaceAll("\"", "");
				System.out.print("contractId = "+contractId+"\t");
			} if (!csvLine[3].isEmpty()){
				publishDate = hm.dateSemiConvert(csvLine[3].substring(0, 9));
				System.out.print("publishDate = "+publishDate+"\t");
			} if (!csvLine[5].isEmpty()){
				startDate = hm.dateSemiConvert(csvLine[5].substring(0, 9));
				System.out.print("startDate = "+startDate+"\t");
			} if (!csvLine[6].isEmpty()){
				endDate = hm.dateSemiConvert(csvLine[6].substring(0, 9));
				System.out.print("endDate = "+endDate+"\t");
			} if (!csvLine[7].isEmpty()){
				value = Double.parseDouble(csvLine[7]);
				System.out.print("value = "+value+"\t");
			} if (!csvLine[8].isEmpty()){
				description = csvLine[8].replaceAll("\"", "");
				System.out.print("description = "+description+"\t");
			} if (!csvLine[9].isEmpty()){
				buyerRefId = csvLine[9].replaceAll("\"", "");
				System.out.print("buyerRefId = "+buyerRefId+"\t");
			} if (!csvLine[10].isEmpty()){
				unspscCode = csvLine[10].replaceAll("\"", "");
				System.out.print("unspscCode = "+unspscCode+"\t");
			} if (csvLine[11] != null && !csvLine[11].isEmpty()){
				unspscSubject = csvLine[11].replaceAll("\"", "");
				System.out.print("unspscSubject = "+unspscSubject+"\t");
			}  if (csvLine[12] != null && !csvLine[12].isEmpty()){
				procurementMethod = csvLine[12].replaceAll("\"", "");
				//hm.findProcurementMethodIndividual(procurementMethod);
				System.out.print("procurementMethod = "+procurementMethod+"\t");
			} if (!csvLine[22].isEmpty()){
				sellerName = csvLine[22].replaceAll("\"", "");
				System.out.print("sellerName = "+sellerName+"\t");
			} if (!csvLine[23].isEmpty()){
				sellerAddress = csvLine[23].replaceAll("\"", "");
				System.out.print("sellerAddress = "+sellerAddress+"\t");
			} if (!csvLine[24].isEmpty()){
				sellerCity = csvLine[24].replaceAll("\"", "");
				System.out.print("sellerCity = "+sellerCity+"\t");
			} if (!csvLine[25].isEmpty()){
				sellerPostcode = csvLine[25].replaceAll("\"", "");
				System.out.print("sellerPostcode = "+sellerPostcode+"\t");
			} if (!csvLine[26].isEmpty()){
				sellerCountry = csvLine[26].replaceAll("\"", "");
				System.out.print("sellerCountry = "+sellerCountry+"\t");
			} if (!csvLine[28].isEmpty()){
				sellerAbn = csvLine[28].replaceAll("\"", "");
				System.out.print("sellerAbn = "+sellerAbn+"\t");
			} if (!csvLine[29].isEmpty()){
				contactName = csvLine[29].replaceAll("\"", "");
				System.out.print("contactName = "+contactName+"\t");
			} if (!csvLine[30].isEmpty()){
				contactPhone = csvLine[30].replaceAll("\"", "");
				System.out.print("contactPhone = "+contactPhone+"\t");
			} if (!csvLine[31].isEmpty()){
				buyerBranch = csvLine[31].replaceAll("\"", "");
				System.out.print("buyerBranch = "+buyerBranch+"\t");
			} if (!csvLine[32].isEmpty()){
				buyerDivision = csvLine[32].replaceAll("\"", "");
				System.out.print("buyerDivision = "+buyerDivision+"\t");
			} if (!csvLine[33].isEmpty()){
				buyerPostcode = csvLine[33].replaceAll("\"", "");
				System.out.print("buyerPostcode = "+buyerPostcode+"\n");
			}
		}
	
		csvObject = new Csv(buyerName,contractId, publishDate, startDate, endDate, value, description, buyerRefId,
				category, procurementMethod, sellerName, sellerAddress, sellerCity, sellerPostcode, sellerCountry,
				sellerAbn, buyerBranch, buyerDivision, buyerPostcode, buyerCountry, unspscCode, unspscSubject, 
				contactName, contactPhone) ;
		
		return csvObject;
	}
}