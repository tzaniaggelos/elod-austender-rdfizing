package ontology;

import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.ResourceFactory;

/**
 * @author A. Tzanis
 */
public class Ontology {

	/** prefixes **/
	public static final String instancePrefix;
	public static final String eLodPrefix;
	public static final String foafPrefix;
	public static final String skosPrefix;
	public static final String orgPrefix;
	public static final String goodRelationsPrefix;	
	public static final String regOrgPrefix;
	public static final String dctermsPrefix;
	public static final String vcardPrefix;
	public static final String publicContractPrefix;
	public static final String elodGeoPrefix;
	
	/** Classes **/
	// eLod
	public static final Resource spendingItemResource;
	public static final Resource expenditureLineResource;
	public static final Resource currencyResource;
	public static final Resource unspscCategoryResource;
	// SKOS
	public static final Resource conceptsResource;
	public static final Resource conceptSchemeResource;
	// GR
	public static final Resource offeringResource;
	public static final Resource unitPriceSpecificationResource;
	public static final Resource someItemsResource;
	public static final Resource businessEntityResource;
	// FOAF
	public static final Resource personResource;
	public static final Resource agencyResource; 
	public static final Resource organizationResource;
	// org
	public static final Resource orgOrganizationResource;
	// rov
	public static final Resource registeredOrganizationResource;
	// vcard
	public static final Resource addressResource;
	// pc
	public static final Resource typeAndQuantityNodeResource;
	public static final Resource contractResource;
	public static final Resource procedureTypeSchemeResource;
	// elodGeo
	public static final Resource countryResource;
	
	/** Object Properties **/
	// eLod
	public static final Property hasExpenditureLine; 			
	public static final Property hasRelatedContract; 
	public static final Property hasSupervisorOrganization; 
	public static final Property isSupervisorOrganizationOf;
	public static final Property buyer;		
	public static final Property amount;		
	public static final Property seller;		
	public static final Property contact;		
	public static final Property hasCurrency;		
	public static final Property hasUNSPSC;		
	//SKOS
	public static final Property hasTopConcept;
	public static final Property inScheme;
	public static final Property topConceptOf;
	// vcard
	public static final Property countryName;				 
	public static final Property hasAddress;				 
	// gr
	public static final Property includesObject;				 
	public static final Property typeOfGood;		
	// pc
	public static final Property actualPrice;		
	public static final Property item;		
	public static final Property procedureType;		
	
	
	/** Data Properties **/
	// eLod
	public static final Property organizationId;				 
	public static final Property division;				 
	public static final Property branch;				 
	public static final Property abn;				 
	public static final Property contractId;				 
	public static final Property currencyIsoCode;				 
	public static final Property unspscSubject;				 
	public static final Property unspscCode;				 
	// SKOS
	public static final Property prefLabel;
	// dcterms	
	public static final Property issued;				 
	// gr
	public static final Property legalName;				 
	public static final Property name;				 
	public static final Property hasCurrencyValue;		
	public static final Property valueAddedTaxIncluded;		
	public static final Property description;		
	public static final Property vatID;		
	// vcard
	public static final Property streetAddress;				 
	public static final Property postalCode;				 
	public static final Property locality;				 
	// pc
	public static final Property startDate;		
	public static final Property estimatedEndDate;		
	// foaf
	public static final Property personPhone;		
	public static final Property personName;
	
	static {
		/** prefixes **/
		instancePrefix = "http://linkedeconomy.org/resource/";
		eLodPrefix = "http://linkedeconomy.org/ontology#";
		dctermsPrefix = "http://purl.org/dc/terms#";
		foafPrefix = "http://xmlns.com/foaf/0.1/";
		skosPrefix = "http://www.w3.org/2004/02/skos/core#";
		orgPrefix = "http://www.w3.org/ns/org#";
		goodRelationsPrefix = "http://purl.org/goodrelations/v1#";
		regOrgPrefix = "http://www.w3.org/ns/regorg#";
		vcardPrefix = "http://www.w3.org/2006/vcard/ns#";
		publicContractPrefix = "http://purl.org/procurement/public-contracts#";
		elodGeoPrefix = "http://linkedeconomy.org/geoOntology#";
		
		/** Resources **/
		
		// eLod
		spendingItemResource = ResourceFactory.createResource(eLodPrefix + "SpendingItem"); 
		expenditureLineResource = ResourceFactory.createResource(eLodPrefix + "ExpenditureLine");
		currencyResource = ResourceFactory.createResource(eLodPrefix + "Currency");
		unspscCategoryResource = ResourceFactory.createResource(eLodPrefix + "UnspscCategory");
		// FOAF
		personResource = ResourceFactory.createResource(foafPrefix + "Person");
		agencyResource = ResourceFactory.createResource(foafPrefix + "Agent");
		organizationResource = ResourceFactory.createResource(foafPrefix + "Organization");
		// SKOS
		conceptsResource = ResourceFactory.createResource(skosPrefix + "Concept");
		conceptSchemeResource = ResourceFactory.createResource(skosPrefix + "ConceptScheme");
		// GR
		businessEntityResource = ResourceFactory.createResource(goodRelationsPrefix + "BusinessEntity");
		offeringResource = ResourceFactory.createResource(goodRelationsPrefix + "Offering");
		unitPriceSpecificationResource = ResourceFactory.createResource(goodRelationsPrefix + "UnitPriceSpecification");
		someItemsResource = ResourceFactory.createResource(goodRelationsPrefix + "SomeItems");
		// vcard
		addressResource = ResourceFactory.createResource(vcardPrefix + "Address");
		// RegOrg
		registeredOrganizationResource = ResourceFactory.createResource(regOrgPrefix + "RegisteredOrganization");
		// org
		orgOrganizationResource = ResourceFactory.createResource(orgPrefix + "Organization");
		// pc
		typeAndQuantityNodeResource = ResourceFactory.createResource(publicContractPrefix + "TypeAndQuantityNode");
		contractResource = ResourceFactory.createResource(publicContractPrefix + "Contract");
		procedureTypeSchemeResource = ResourceFactory.createResource(publicContractPrefix + "ProcedureTypeScheme");
		// elodGeo
		countryResource = ResourceFactory.createResource(elodGeoPrefix + "Country");
		
		/** Object Properties **/
		// eLod
		hasExpenditureLine = ResourceFactory.createProperty(eLodPrefix + "hasExpenditureLine");			
		hasRelatedContract = ResourceFactory.createProperty(eLodPrefix + "hasRelatedContract"); 
		hasSupervisorOrganization = ResourceFactory.createProperty(eLodPrefix + "hasSupervisorOrganization");
		isSupervisorOrganizationOf = ResourceFactory.createProperty(eLodPrefix + "isSupervisorOrganizationOf");
		buyer = ResourceFactory.createProperty(eLodPrefix + "buyer");		
		amount = ResourceFactory.createProperty(eLodPrefix + "amount");		
		seller = ResourceFactory.createProperty(eLodPrefix + "seller");		
		contact = ResourceFactory.createProperty(eLodPrefix + "contact");		
		hasCurrency = ResourceFactory.createProperty(eLodPrefix + "hasCurrency");		
		hasUNSPSC = ResourceFactory.createProperty(eLodPrefix + "hasUNSPSC");
		//SKOS
		hasTopConcept = ResourceFactory.createProperty(skosPrefix + "hasTopConcept");
		inScheme = ResourceFactory.createProperty(skosPrefix + "inScheme");
		topConceptOf = ResourceFactory.createProperty(skosPrefix + "topConceptOf");
		// vcard
		countryName = ResourceFactory.createProperty(vcardPrefix + "country-name");				 
		hasAddress = ResourceFactory.createProperty(vcardPrefix + "hasAddress");				 
		// gr
		includesObject = ResourceFactory.createProperty(goodRelationsPrefix + "includesObject");				 
		typeOfGood = ResourceFactory.createProperty(goodRelationsPrefix + "typeOfGood");		
		// pc
		actualPrice = ResourceFactory.createProperty(publicContractPrefix + "actualPrice");		
		item = ResourceFactory.createProperty(publicContractPrefix + "item");		
		procedureType = ResourceFactory.createProperty(publicContractPrefix + "procedureType");		
		
		/** Data Properties **/
		// eLod
		organizationId = ResourceFactory.createProperty(eLodPrefix + "organizationId");
		division = ResourceFactory.createProperty(eLodPrefix + "division");
		branch = ResourceFactory.createProperty(eLodPrefix + "branch");
		abn = ResourceFactory.createProperty(eLodPrefix + "abn");
		contractId = ResourceFactory.createProperty(eLodPrefix + "contractId");
		currencyIsoCode = ResourceFactory.createProperty(eLodPrefix + "currencyIsoCode");
		unspscSubject = ResourceFactory.createProperty(eLodPrefix + "unspscSubject");
		unspscCode = ResourceFactory.createProperty(eLodPrefix + "unspscCode");
		// SKOS
		prefLabel = ResourceFactory.createProperty(skosPrefix + "prefLabel");
		// dcterms
		issued = ResourceFactory.createProperty(dctermsPrefix + "issued");
		// gr
		legalName = ResourceFactory.createProperty(goodRelationsPrefix + "legalName");				 
		name = ResourceFactory.createProperty(goodRelationsPrefix + "name");				 
		hasCurrencyValue = ResourceFactory.createProperty(goodRelationsPrefix + "hasCurrencyValue");		
		valueAddedTaxIncluded = ResourceFactory.createProperty(goodRelationsPrefix + "valueAddedTaxIncluded");		
		description = ResourceFactory.createProperty(goodRelationsPrefix + "description");		
		vatID = ResourceFactory.createProperty(goodRelationsPrefix + "vatID");
		// vcard
		streetAddress = ResourceFactory.createProperty(vcardPrefix + "street-address");				 
		postalCode = ResourceFactory.createProperty(vcardPrefix + "postal-code");				 
		locality = ResourceFactory.createProperty(vcardPrefix + "locality");				 
		// pc
		startDate = ResourceFactory.createProperty(publicContractPrefix + "startDate");		
		estimatedEndDate = ResourceFactory.createProperty(publicContractPrefix + "estimatedEndDate");		
		// foaf
		personPhone = ResourceFactory.createProperty(foafPrefix + "personPhone");		
		personName = ResourceFactory.createProperty(foafPrefix + "personName");
		
	}
}