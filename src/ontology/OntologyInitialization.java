package ontology;

import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.vocabulary.OWL;
import com.hp.hpl.jena.vocabulary.RDFS;

/**
 * @author A. Tzani
 */
public class OntologyInitialization {
	
/**
  * Add the necessary prefixes to the model we are currently 
  * working with.
  * 
  * @param Model the model2 we are currently working with
  */
	public void setPrefixes(Model model1) {
		
		/** Model1 **/
    	if (model1 != null){
    		model1.setNsPrefix("elod", Ontology.eLodPrefix);
    		model1.setNsPrefix("instance", Ontology.instancePrefix);
    		model1.setNsPrefix("skos", Ontology.skosPrefix);
    		model1.setNsPrefix("gr", Ontology.goodRelationsPrefix);
    		model1.setNsPrefix("rov", Ontology.regOrgPrefix);
    		model1.setNsPrefix("org", Ontology.orgPrefix);
    		model1.setNsPrefix("foaf", Ontology.foafPrefix);
    		model1.setNsPrefix("dcterms", Ontology.dctermsPrefix);
    		model1.setNsPrefix("pc", Ontology.publicContractPrefix);
    		model1.setNsPrefix("elodGeo", Ontology.elodGeoPrefix);
    		model1.setNsPrefix("vcard", Ontology.vcardPrefix);
    	}
	}
	
	/**
	 * Create the basic hierarchies of the OWL classes and their labels 
	 * to the model we are currently working with.
	 * 
	 * @param Model the model1 we are currently working with
	 * @param Model the model2 we are currently working with
	 */
	public void createHierarchies(Model model1) {
		
		/** Model1 **/
		
			//Agency
			model1.add(Ontology.agencyResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.agencyResource, RDFS.label, model1.createLiteral("Agency", "en"));
			model1.add(Ontology.agencyResource, RDFS.label, model1.createLiteral("��������", "el"));
			
			//Agency - Organization (FOAF)
			model1.add(Ontology.organizationResource, RDFS.subClassOf, Ontology.agencyResource);
			model1.add(Ontology.organizationResource, RDFS.label, model1.createLiteral("Organization", "en"));
			model1.add(Ontology.organizationResource, RDFS.label, model1.createLiteral("����������", "el"));
		
			//Agency - BusinessEntity
			model1.add(Ontology.businessEntityResource, RDFS.subClassOf, Ontology.agencyResource);
			model1.add(Ontology.businessEntityResource, RDFS.label, model1.createLiteral("Business Entity", "en"));
			model1.add(Ontology.businessEntityResource, RDFS.label, model1.createLiteral("�������������� ��������", "el"));
		
			//Agency - Organization (org)
			model1.add(Ontology.orgOrganizationResource, RDFS.subClassOf, Ontology.agencyResource);
			model1.add(Ontology.orgOrganizationResource, RDFS.label, model1.createLiteral("Organization", "en"));
			model1.add(Ontology.orgOrganizationResource, RDFS.label, model1.createLiteral("����������", "el"));
		
			//Agency - Registered Organization
			model1.add(Ontology.registeredOrganizationResource, RDFS.subClassOf, Ontology.agencyResource);
			model1.add(Ontology.registeredOrganizationResource, RDFS.label, model1.createLiteral("Registered Organization", "en"));
			model1.add(Ontology.registeredOrganizationResource, RDFS.label, model1.createLiteral("������������� ����������", "el"));
		
			//Address
			model1.add(Ontology.addressResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.addressResource, RDFS.label, model1.createLiteral("Address", "en"));
			model1.add(Ontology.addressResource, RDFS.label, model1.createLiteral("���������", "el"));

			//SpendingItem
			model1.add(Ontology.spendingItemResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.spendingItemResource, RDFS.label, model1.createLiteral("Spending Item", "en"));
			model1.add(Ontology.spendingItemResource, RDFS.label, model1.createLiteral("����������� �������", "el"));

			//ExpenditureLine
			model1.add(Ontology.expenditureLineResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.expenditureLineResource, RDFS.label, model1.createLiteral("Expenditure Line", "en"));
			model1.add(Ontology.expenditureLineResource, RDFS.label, model1.createLiteral("����� �������", "el"));

			//Contract
			model1.add(Ontology.contractResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.contractResource, RDFS.label, model1.createLiteral("Public Contract", "en"));
			model1.add(Ontology.contractResource, RDFS.label, model1.createLiteral("������� �������", "el"));
			
			//UnitPriceSpecification
			model1.add(Ontology.unitPriceSpecificationResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.unitPriceSpecificationResource, RDFS.label, model1.createLiteral("Unit Price Specification", "en"));
			model1.add(Ontology.unitPriceSpecificationResource, RDFS.label, model1.createLiteral("����������� ����� ��� ������", "el"));

			//Offering
			model1.add(Ontology.offeringResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.offeringResource, RDFS.label, model1.createLiteral("Offering", "en"));
			model1.add(Ontology.offeringResource, RDFS.label, model1.createLiteral("��������", "el"));

			//Person
			model1.add(Ontology.personResource, RDFS.subClassOf, Ontology.agencyResource);
			model1.add(Ontology.personResource, RDFS.label, model1.createLiteral("Person", "en"));
			model1.add(Ontology.personResource, RDFS.label, model1.createLiteral("�������", "el"));

			//TypeAndQuantityNode
			model1.add(Ontology.typeAndQuantityNodeResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.typeAndQuantityNodeResource, RDFS.label, model1.createLiteral("Type And Quantity Node", "en"));
			model1.add(Ontology.typeAndQuantityNodeResource, RDFS.label, model1.createLiteral("����� ��� ��������", "el"));

			//SomeItems
			model1.add(Ontology.someItemsResource, RDFS.subClassOf, OWL.Thing);
			model1.add(Ontology.someItemsResource, RDFS.label, model1.createLiteral("Some Items", "en"));
			model1.add(Ontology.someItemsResource, RDFS.label, model1.createLiteral("�������� ��������", "el"));
			
			//Concept
			model1.add(Ontology.conceptsResource, RDFS.subClassOf, OWL.Thing);
			
			//ConceptScheme
			model1.add(Ontology.conceptSchemeResource, RDFS.subClassOf, OWL.Thing);

			//Concept - ProcurementMethod
			//model1.add(Ontology.procurementMethodResource, RDFS.subClassOf, Ontology.conceptsResource);
			//model1.add(Ontology.procurementMethodResource, RDFS.label, model1.createLiteral("Procurement Method", "en"));
			//model1.add(Ontology.procurementMethodResource, RDFS.label, model1.createLiteral("������� ��� ��� �������� ���������", "el"));

			//Concept - Currency
			model1.add(Ontology.currencyResource, RDFS.subClassOf, Ontology.conceptsResource);
			model1.add(Ontology.currencyResource, RDFS.label, model1.createLiteral("Currency", "en"));
			model1.add(Ontology.currencyResource, RDFS.label, model1.createLiteral("�������", "el"));

			//Concept - Country
			model1.add(Ontology.countryResource, RDFS.subClassOf, Ontology.conceptsResource);
			model1.add(Ontology.countryResource, RDFS.label, model1.createLiteral("Country", "en"));
			model1.add(Ontology.countryResource, RDFS.label, model1.createLiteral("����", "el"));
			
	}
}