package objects;


/**
 * @author A. Tzani
 */
public class Csv {
	
	public String buyerName = null; 							// Buyer name					i=0
	public String contractId = null;							// Contract id					i=2
	public String publishDate = null;							// Contract publish date		i=3
	public String startDate = null;								// Contract start date			i=6
	public String endDate = null;								// Contract end date			i=7
	public double value = 0.00;									// Contract value				i=8
	public String description = null;							// Contract description			i=9
	public String buyerRefId = null;							// Buyer ref. id				i=10
	public String category = null;								// Contract category			i=11
	public String procurementMethod = null;						// Procurement method			i=12
	public String sellerName = null;							// Seller name					i=22
	public String sellerAddress = null;							// Seller address				i=23
	public String sellerCity = null;							// Seller city					i=24
	public String sellerPostcode = null;						// Seller post-code				i=25
	public String sellerCountry = null;							// Seller country				i=26
	public String sellerAbn = null;								// Seller ABN 					i=28
	public String buyerBranch = null;							// Buyer branch					i=29
	public String buyerDivision = null;							// Buyer division				i=30
	public String buyerPostcode = null;							// Buyer post-code				i=31
	public String buyerCountry = "AUSTRALIA";					// Buyer country
	public String unspscCode = null;							// Unspsc Code
	public String unspscSubject = null;							// Unspsc Subject
	public String phone = null;									// person phone
	public String personName = null;							// person name
		
	public Csv(String buyerName,String contractId, String publishDate, String startDate, String endDate, double value, String description, String buyerRefId,
			String category, String procurementMethod, String sellerName, String sellerAddress, String sellerCity, String sellerPostcode, String sellerCountry,
			String sellerAbn, String buyerBranch, String buyerDivision, String buyerPostcode, String buyerCountry, String unspscCode, String unspscSubject,
			String personName, String phone) {
		
		this.buyerName = buyerName;
		this.contractId = contractId;
		this.publishDate = publishDate;
		this.startDate = startDate;
		this.endDate = endDate;
		this.value = value;
		this.description = description;
		this.buyerRefId = buyerRefId;
		this.category = category;
		this.procurementMethod = procurementMethod;
		this.sellerName = sellerName;
		this.sellerAddress = sellerAddress;
		this.sellerCity = sellerCity;
		this.sellerPostcode = sellerPostcode;
		this.sellerCountry = sellerCountry;
		this.sellerAbn = sellerAbn;
		this.buyerBranch = buyerBranch;
		this.buyerDivision = buyerDivision;
		this.buyerPostcode = buyerPostcode;
		this.buyerCountry = buyerCountry;
		this.unspscCode = unspscCode;
		this.unspscSubject = unspscSubject;
		this.phone = phone;
		this.personName = personName;
						
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName.toUpperCase();
	}

	public String getContractId() {
		return contractId;
	}

	public void setContractId(String contractId) {
		this.contractId = contractId.toUpperCase();
	}

	public String getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(String publishDate) {
		this.publishDate = publishDate;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	
	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description.toUpperCase();
	}

	public String getBuyerRefId() {
		return buyerRefId;
	}

	public void setBuyerRefId(String buyerRefId) {
		this.buyerRefId = buyerRefId.toUpperCase();
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category.toUpperCase();
	}

	public String getProcurementMethod() {
		return procurementMethod;
	}

	public void setProcurementMethod(String procurementMethod) {
		this.procurementMethod = procurementMethod.toUpperCase();
	}

	public String getSellerName() {
		return sellerName;
	}

	public void setSellerName(String sellerName) {
		this.sellerName = sellerName.toUpperCase();
	}

	public String getSellerAddress() {
		return sellerAddress;
	}

	public void setSellerAddress(String sellerAddress) {
		this.sellerAddress = sellerAddress.toUpperCase();
	}

	public String getSellerCity() {
		return sellerCity;
	}

	public void setSellerCity(String sellerCity) {
		this.sellerCity = sellerCity.toUpperCase();
	}
	
	public String getSellerPostcode() {
		return sellerPostcode;
	}

	public void setSellerPostcode(String sellerPostcode) {
		this.sellerPostcode = sellerPostcode;
	}
	
	public String getSellerCountry() {
		return sellerCountry;
	}

	public void setSellerCountry(String sellerCountry) {
		this.sellerCountry = sellerCountry.toUpperCase();
	}
	
	public String getSellerAbn() {
		return sellerAbn;
	}

	public void setSellerAbn(String sellerAbn) {
		this.sellerAbn = sellerAbn.toUpperCase();
	}
	
	public String getBuyerBranch() {
		return buyerBranch;
	}

	public void setBuyerBranch(String buyerBranch) {
		this.buyerBranch = buyerBranch.toUpperCase();
	}
	
	public String getBuyerDivision() {
		return buyerDivision;
	}

	public void setBuyerDivision(String buyerDivision) {
		this.buyerDivision = buyerDivision.toUpperCase();
	}
	
	public String getBuyerPostcode() {
		return buyerPostcode;
	}

	public void setBuyerPostcode(String buyerPostcode) {
		this.buyerPostcode = buyerPostcode;
	}
	
	public String getBuyerCountry() {
		return buyerCountry;
	}

	public void setBuyerCountry(String buyerCountry) {
		this.buyerCountry = buyerCountry.toUpperCase();
	}

	public String getUnspscCode() {
		return unspscCode;
	}

	public void setUnspscCode(String unspscCode) {
		this.unspscCode = unspscCode.toUpperCase();
	}

	public String getUnspscSubject() {
		return unspscSubject;
	}

	public void setUnspscSubject(String unspscSubject) {
		this.unspscCode = unspscSubject.toUpperCase();
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName.toUpperCase();
	}
	
}
